'use strict'
const { url } = require('inspector')
// Template version: 1.3.1
// see http://vuejs-templates.github.io/webpack for documentation.

const path = require('path')



const staticUrl = "http://localhost:8989/"

module.exports = {
  dev: {

    // Paths
    // URLS :"http://110.42.188.221:8989",
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {
      
    '/api':{
      target: staticUrl,
      changeOrigin:true,
      pathRewrite:{
          '^/api':''
      },
  },
  '/school':{
    target:staticUrl,
    changeOrigin:true,
    pathRewrite:{
      '^/school':''
    },
  } ,
  '/login':{
    target:staticUrl,
    changeOrigin:true,
    pathRewrite:{
      '^/login':''
    },

  } ,

  '/hphotos':{
    target:staticUrl,
    changeOrigin:true,
    pathRewrite:{
      '^/hphotos':''
    },

  } ,
  '/CampusScenery':{
    target:staticUrl,
    changeOrigin:true,
    pathRewrite:{
      '^/CampusScenery':''
    },
  } ,
  '/userInfo':{
    target:staticUrl,
    changeOrigin:true,
    pathRewrite:{
      '^/userInfo':''
    },
  },
  '/specialized':{
    target:staticUrl,
    changeOrigin:true,
    pathRewrite:{
      '^/specialized':''
    },
  },
  '/SchoolImg':{
    target:staticUrl,
    changeOrigin:true,
    pathRewrite:{
      '^/SchoolImg':''
    },
  },
  '/Volunteer':{
    target:staticUrl,
    changeOrigin:true,
    pathRewrite:{
      '^/Volunteer':''
    },
    // logout
    // '/logout':{
    //   target:"http://110.42.188.221:8989",
    //   changeOrigin:true,
    //   pathRewrite:{
    //     '^/logout':''
    //   },
  }
  
},



    

    // Various Dev Server settings
    host: 'localhost', // can be overwritten by process.env.HOST
    port: 80, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
    autoOpenBrowser: false,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-

    

   
    /**
     * Source Maps
     */

    // https://webpack.js.org/configuration/devtool/#development
    devtool: 'cheap-module-eval-source-map',

    // If you have problems debugging vue-files in devtools,
    // set this to false - it *may* help
    // https://vue-loader.vuejs.org/en/options.html#cachebusting
    cacheBusting: true,


  },


//   dev: {

//     // Paths
//     // URLS :"http://110.42.188.221:8989",
//     assetsSubDirectory: 'static',
//     assetsPublicPath: '/',
//     proxyTable: {
      
//     '/api':{
//       target: staticUrl,
//       changeOrigin:true,
//       pathRewrite:{
//           '^/api':''
//       },
//   },
//   '/school':{
//     target:staticUrl,
//     changeOrigin:true,
//     pathRewrite:{
//       '^/school':''
//     },
//   } ,
//   '/login':{
//     target:staticUrl,
//     changeOrigin:true,
//     pathRewrite:{
//       '^/login':''
//     },

//   } ,

//   '/hphotos':{
//     target:staticUrl,
//     changeOrigin:true,
//     pathRewrite:{
//       '^/hphotos':''
//     },

//   } ,
//   '/CampusScenery':{
//     target:staticUrl,
//     changeOrigin:true,
//     pathRewrite:{
//       '^/CampusScenery':''
//     },
//   } ,
//   '/userInfo':{
//     target:staticUrl,
//     changeOrigin:true,
//     pathRewrite:{
//       '^/userInfo':''
//     },
//   },
//   '/specialized':{
//     target:staticUrl,
//     changeOrigin:true,
//     pathRewrite:{
//       '^/specialized':''
//     },
//   },
//   '/SchoolImg':{
//     target:staticUrl,
//     changeOrigin:true,
//     pathRewrite:{
//       '^/SchoolImg':''
//     },
//   },
//   '/Volunteer':{
//     target:staticUrl,
//     changeOrigin:true,
//     pathRewrite:{
//       '^/Volunteer':''
//     },
//   }
  
// },



    

//     // Various Dev Server settings
//     host: 'localhost', // can be overwritten by process.env.HOST
//     port: 80, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
//     autoOpenBrowser: false,
//     errorOverlay: true,
//     notifyOnErrors: true,
//     poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-

    

   
//     /**
//      * Source Maps
//      */

//     // https://webpack.js.org/configuration/devtool/#development
//     devtool: 'cheap-module-eval-source-map',

//     // If you have problems debugging vue-files in devtools,
//     // set this to false - it *may* help
//     // https://vue-loader.vuejs.org/en/options.html#cachebusting
//     cacheBusting: true,


//   },






  build: {
    //添加test，pord环境变量配置
    prodEvn: require('./prod.env'),
    testEvn: require('./test.evn'),
    // Template for index.html
    index: path.resolve(__dirname, '../dist/index.html'),

    // Paths
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: './',

    /**
     * Source Maps
     */

    productionSourceMap: true,
    // https://webpack.js.org/configuration/devtool/#production
    devtool: '#source-map',

    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],

    // Run the build command with an extra argument to
    // View the bundle analyzer report after build finishes:
    // `npm run build --report`
    // Set to `true` or `false` to always turn it on or off
    bundleAnalyzerReport: process.env.npm_config_report
  }
}
