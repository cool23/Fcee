
// resize.js
const scaleListener = () => {
    window.addEventListener('resize', resize)
    console.log('scaleListening......')
  }
  const resize = () => {
    // 与原来 1080 的比值
    let scale = window.innerHeight / 192
    document.documentElement.style.fontSize = `${16 * scale}px` 
    console.log('resize')
  }
  export {
    scaleListener
  }
