import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/index'
import zhuanjia from '@/components/zhuanjia'
import yonghu from "@/components/yonghu"
import zhuanye from "@/components/zhuanye"
import moni from "@/components/moni"
import schoolinfo from "@/components/schoolinfo"
import myVolunteer from "@/components/myVolunteer"
import erro from '@/components/erro'
import spinfo from "@/components/spinfo"
import consult from "@/components/consult"
import onepice from "@/components/openpice"
Vue.use(Router)

export default new Router({
  mode:'hash',

  routes: [
    {
      
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path:'/zhuanjia',
      name:'zhuanjia',
      component:zhuanjia
    },
    {
      path:'/yonghu',
      name:'yonghu',
      component:yonghu
    },
    {
      path:'/zhuanye',
      name:'/zhuanye',
      component:zhuanye
    },
    {
      path:'/moni',
      name:'/moni',
      component:moni
    },
    {
      path: '/schoolinfo',
      name: 'schoolinfo',
      component: schoolinfo
    },
    {
      path: '/myVolunteer',
      name: 'myVolunteer',
      component: myVolunteer
    },
    {
      path: '/erro',
      name: 'erro',
      component: erro
    },
    // spinfo
    {
      path: '/spinfo',
      name: 'spinfo',
      component: spinfo
    },
    {
      path:'/consult',
      name:'consult',
      component:consult
    },
    // 
    {
      path:'/openpice',
      name:'openpice',
      component:onepice
    }
  ]
})


Vue.component('com3',{
  template:'<h1>组件3</h1>'
})
Vue.component('com4',{
  template:'<h1>组件4</h1>'
})
Vue.component('com5',{
  template:'<h1>组件5</h1>'
})
Vue.component('com6',{
  template:'<h1>组件6</h1>'
})
Vue.component('com7',{
  template:'<h1>组件7</h1>'
})
Vue.component('com8',{
  template:'<h1>组件8</h1>'
})
