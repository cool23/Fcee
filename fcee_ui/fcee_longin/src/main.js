// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import '@/style/index.css'
import Axios from 'axios'
import 'lib-flexible/flexible'
import "./components/tool/rem"
// import tabbar from "./components/fceecomponents"
Vue.prototype.$axios = Axios
Axios.defaults.baseURL = '/api'
Axios.defaults.headers.post['Content-Type'] = 'application/json';
Vue.config.productionTip = false


Vue.use(ElementUI);
// Vue.component("topbar",tabbar)
/* eslint-disable no-new */

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  reader: h=>h(App)
})
