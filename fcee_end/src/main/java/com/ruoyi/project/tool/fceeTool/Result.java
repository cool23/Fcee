package com.ruoyi.project.tool.fceeTool;

import org.apache.poi.ss.formula.functions.T;

public class Result<T> {
    private Integer code;//状态码
    private T data;//数据
    private  String masse;//消息

    public Result(Integer code, T data) {
        this.code = code;
        this.data = data;
    }

    public Result(Integer code, T data, String masse) {
        this.code = code;
        this.data = data;
        this.masse = masse;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMasse() {
        return masse;
    }

    public void setMasse(String masse) {
        this.masse = masse;
    }
}
