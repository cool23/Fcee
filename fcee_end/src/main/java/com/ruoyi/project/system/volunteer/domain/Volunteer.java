package com.ruoyi.project.system.volunteer.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 我的志愿单对象 volunteer
 * 
 * @author sw
 * @date 2022-08-25
 */
public class Volunteer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /**  */
    @Excel(name = "")
    private String userName;

    /** 用户id */
    @Excel(name = "用户id")
    private String UserId;

    /** 志愿院校 */
    @Excel(name = "志愿院校")
    private String VolunteerColleges;

    /** 志愿专业 */
    @Excel(name = "志愿专业")
    private String Volunteering;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getUserId()
    {
        return UserId;
    }
    public void setVolunteerColleges(String VolunteerColleges)
    {
        this.VolunteerColleges = VolunteerColleges;
    }

    public String getVolunteerColleges()
    {
        return VolunteerColleges;
    }
    public void setVolunteering(String Volunteering)
    {
        this.Volunteering = Volunteering;
    }

    public String getVolunteering()
    {
        return Volunteering;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userName", getUserName())
            .append("UserId", getUserId())
            .append("VolunteerColleges", getVolunteerColleges())
            .append("Volunteering", getVolunteering())
            .toString();
    }
}
