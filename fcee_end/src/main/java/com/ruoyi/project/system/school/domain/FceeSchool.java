package com.ruoyi.project.system.school.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.springframework.stereotype.Component;

/**
 * 院校管理对象 fcee_school
 * 
 * @author 宋伟
 * @date 2022-05-22
 */

public class FceeSchool extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long sid;

    /** 院校代号 */
    @Excel(name = "院校代号")
    private String SchoolCode;

    /** 院校名称 */
    @Excel(name = "院校名称")
    private String SchoolName;

    /** 省份 */
    @Excel(name = "省份")
    private String province;

    /** 办学类型 */
    @Excel(name = "办学类型")
    private String SchoolType;

    /** 总计划数 */
    @Excel(name = "总计划数")
    private String totalNumberOfPlans;

    /** 专业代号 */
    @Excel(name = "专业代号")
    private String ProfessionalCode;

    /** 专业名称（类及备注） */
    @Excel(name = "专业名称", readConverterExp = "类=及备注")
    private String ProfessionalName;

    /** 选考科目要求 */
    @Excel(name = "选考科目要求")
    private String ElectiveSubjectRequirements;

    /** 学制 */
    @Excel(name = "学制")
    private String AcademicSystem;

    /** 计划数 */
    @Excel(name = "计划数")
    private String numberOfPlans;

    /** 年收费（元） */
    @Excel(name = "年收费", readConverterExp = "元=")
    private String annualFee;


    public void setSid(Long sid)
    {
        this.sid = sid;
    }

    public Long getSid()
    {
        return sid;
    }
    public void setSchoolCode(String SchoolCode)
    {
        this.SchoolCode = SchoolCode;
    }

    public String getSchoolCode()
    {
        return SchoolCode;
    }
    public void setSchoolName(String SchoolName)
    {
        this.SchoolName = SchoolName;
    }

    public String getSchoolName()
    {
        return SchoolName;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getProvince()
    {
        return province;
    }
    public void setSchoolType(String SchoolType)
    {
        this.SchoolType = SchoolType;
    }

    public String getSchoolType()
    {
        return SchoolType;
    }
    public void setTotalNumberOfPlans(String totalNumberOfPlans)
    {
        this.totalNumberOfPlans = totalNumberOfPlans;
    }

    public String getTotalNumberOfPlans()
    {
        return totalNumberOfPlans;
    }
    public void setProfessionalCode(String ProfessionalCode)
    {
        this.ProfessionalCode = ProfessionalCode;
    }

    public String getProfessionalCode()
    {
        return ProfessionalCode;
    }
    public void setProfessionalName(String ProfessionalName)
    {
        this.ProfessionalName = ProfessionalName;
    }

    public String getProfessionalName()
    {
        return ProfessionalName;
    }
    public void setElectiveSubjectRequirements(String ElectiveSubjectRequirements)
    {
        this.ElectiveSubjectRequirements = ElectiveSubjectRequirements;
    }

    public String getElectiveSubjectRequirements()
    {
        return ElectiveSubjectRequirements;
    }
    public void setAcademicSystem(String AcademicSystem)
    {
        this.AcademicSystem = AcademicSystem;
    }

    public String getAcademicSystem()
    {
        return AcademicSystem;
    }
    public void setNumberOfPlans(String numberOfPlans)
    {
        this.numberOfPlans = numberOfPlans;
    }

    public String getNumberOfPlans()
    {
        return numberOfPlans;
    }
    public void setAnnualFee(String annualFee)
    {
        this.annualFee = annualFee;
    }

    public String getAnnualFee()
    {
        return annualFee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("sid", getSid())
            .append("SchoolCode", getSchoolCode())
            .append("SchoolName", getSchoolName())
            .append("province", getProvince())
            .append("SchoolType", getSchoolType())
            .append("totalNumberOfPlans", getTotalNumberOfPlans())
            .append("ProfessionalCode", getProfessionalCode())
            .append("ProfessionalName", getProfessionalName())
            .append("ElectiveSubjectRequirements", getElectiveSubjectRequirements())
            .append("AcademicSystem", getAcademicSystem())
            .append("numberOfPlans", getNumberOfPlans())
            .append("annualFee", getAnnualFee())
            .toString();
    }
}
