package com.ruoyi.project.system.schoolimgs.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.schoolimgs.domain.Schoolimgs;
import com.ruoyi.project.system.schoolimgs.service.ISchoolimgsService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 院校封面图片Controller
 * 
 * @author ruoyi
 * @date 2022-07-10
 */
@Controller
@RequestMapping("/system/schoolimgs")
public class SchoolimgsController extends BaseController
{
    private String prefix = "system/schoolimgs";

    @Autowired
    private ISchoolimgsService schoolimgsService;

    @RequiresPermissions("system:schoolimgs:view")
    @GetMapping()
    public String schoolimgs()
    {
        return prefix + "/schoolimgs";
    }

    /**
     * 查询院校封面图片列表
     */
    @RequiresPermissions("system:schoolimgs:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Schoolimgs schoolimgs)
    {
        startPage();
        List<Schoolimgs> list = schoolimgsService.selectSchoolimgsList(schoolimgs);
        return getDataTable(list);
    }

    /**
     * 导出院校封面图片列表
     */
    @RequiresPermissions("system:schoolimgs:export")
    @Log(title = "院校封面图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Schoolimgs schoolimgs)
    {
        List<Schoolimgs> list = schoolimgsService.selectSchoolimgsList(schoolimgs);
        ExcelUtil<Schoolimgs> util = new ExcelUtil<Schoolimgs>(Schoolimgs.class);
        return util.exportExcel(list, "院校封面图片数据");
    }

    /**
     * 新增院校封面图片
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存院校封面图片
     */
    @RequiresPermissions("system:schoolimgs:add")
    @Log(title = "院校封面图片", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Schoolimgs schoolimgs)
    {
        return toAjax(schoolimgsService.insertSchoolimgs(schoolimgs));
    }

    /**
     * 修改院校封面图片
     */
    @RequiresPermissions("system:schoolimgs:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Schoolimgs schoolimgs = schoolimgsService.selectSchoolimgsById(id);
        mmap.put("schoolimgs", schoolimgs);
        return prefix + "/edit";
    }

    /**
     * 修改保存院校封面图片
     */
    @RequiresPermissions("system:schoolimgs:edit")
    @Log(title = "院校封面图片", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Schoolimgs schoolimgs)
    {
        return toAjax(schoolimgsService.updateSchoolimgs(schoolimgs));
    }

    /**
     * 删除院校封面图片
     */
    @RequiresPermissions("system:schoolimgs:remove")
    @Log(title = "院校封面图片", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(schoolimgsService.deleteSchoolimgsByIds(ids));
    }
}
