package com.ruoyi.project.system.count.mapper;

import java.util.List;
import com.ruoyi.project.system.count.domain.FceeCount;

/**
 * 用户信息Mapper接口
 * 
 * @author songwei
 * @date 2022-06-09
 */
public interface FceeCountMapper 
{
    /**
     * 查询用户信息
     * 
     * @param id 用户信息主键
     * @return 用户信息
     */
    public FceeCount selectFceeCountById(Long id);

    /**
     * 查询用户信息列表
     * 
     * @param fceeCount 用户信息
     * @return 用户信息集合
     */
    public List<FceeCount> selectFceeCountList(FceeCount fceeCount);

    /**
     * 新增用户信息
     * 
     * @param fceeCount 用户信息
     * @return 结果
     */
    public int insertFceeCount(FceeCount fceeCount);

    /**
     * 修改用户信息
     * 
     * @param fceeCount 用户信息
     * @return 结果
     */
    public int updateFceeCount(FceeCount fceeCount);

    /**
     * 删除用户信息
     * 
     * @param id 用户信息主键
     * @return 结果
     */
    public int deleteFceeCountById(Long id);

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFceeCountByIds(String[] ids);
}
