package com.ruoyi.project.system.school.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.school.domain.FceeSchool;
import com.ruoyi.project.system.school.service.IFceeSchoolService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 院校管理Controller
 * 
 * @author 宋伟
 * @date 2022-05-22
 */
@Controller
@RequestMapping("/system/school")
public class FceeSchoolController extends BaseController
{
    private String prefix = "system/school";

    @Autowired
    private IFceeSchoolService fceeSchoolService;

    @RequiresPermissions("system:school:view")
    @GetMapping()
    public String school()
    {
        return prefix + "/school";
    }

    /**
     * 查询院校管理列表
     */
    @RequiresPermissions("system:school:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(FceeSchool fceeSchool)
    {
        startPage();
        List<FceeSchool> list = fceeSchoolService.selectFceeSchoolList(fceeSchool);
        return getDataTable(list);
    }

    /**
     * 导出院校管理列表
     */
    @RequiresPermissions("system:school:export")
    @Log(title = "院校管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(FceeSchool fceeSchool)
    {
        List<FceeSchool> list = fceeSchoolService.selectFceeSchoolList(fceeSchool);
        ExcelUtil<FceeSchool> util = new ExcelUtil<FceeSchool>(FceeSchool.class);
        return util.exportExcel(list, "院校管理数据");
    }

    /**
     * 新增院校管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存院校管理
     */
    @RequiresPermissions("system:school:add")
    @Log(title = "院校管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(FceeSchool fceeSchool)
    {
        return toAjax(fceeSchoolService.insertFceeSchool(fceeSchool));
    }

    /**
     * 修改院校管理
     */
    @RequiresPermissions("system:school:edit")
    @GetMapping("/edit/{sid}")
    public String edit(@PathVariable("sid") Long sid, ModelMap mmap)
    {
        FceeSchool fceeSchool = fceeSchoolService.selectFceeSchoolBySid(sid);
        mmap.put("fceeSchool", fceeSchool);
        return prefix + "/edit";
    }

    /**
     * 修改保存院校管理
     */
    @RequiresPermissions("system:school:edit")
    @Log(title = "院校管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(FceeSchool fceeSchool)
    {
        return toAjax(fceeSchoolService.updateFceeSchool(fceeSchool));
    }

    /**
     * 删除院校管理
     */
    @RequiresPermissions("system:school:remove")
    @Log(title = "院校管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(fceeSchoolService.deleteFceeSchoolBySids(ids));
    }
}
