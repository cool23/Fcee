package com.ruoyi.project.system.count.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.count.domain.FceeCount;
import com.ruoyi.project.system.count.service.IFceeCountService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户信息Controller
 * 
 * @author songwei
 * @date 2022-06-09
 */
@Controller
@RequestMapping("/system/count")
public class FceeCountController extends BaseController
{
    private String prefix = "system/count";

    @Autowired
    private IFceeCountService fceeCountService;

    @RequiresPermissions("system:count:view")
    @GetMapping()
    public String count()
    {
        return prefix + "/count";
    }

    /**
     * 查询用户信息列表
     */
    @RequiresPermissions("system:count:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(FceeCount fceeCount)
    {
        startPage();
        List<FceeCount> list = fceeCountService.selectFceeCountList(fceeCount);
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    @RequiresPermissions("system:count:export")
    @Log(title = "用户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(FceeCount fceeCount)
    {
        List<FceeCount> list = fceeCountService.selectFceeCountList(fceeCount);
        ExcelUtil<FceeCount> util = new ExcelUtil<FceeCount>(FceeCount.class);
        return util.exportExcel(list, "用户信息数据");
    }

    /**
     * 新增用户信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户信息
     */
    @RequiresPermissions("system:count:add")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(FceeCount fceeCount)
    {
        return toAjax(fceeCountService.insertFceeCount(fceeCount));
    }

    /**
     * 修改用户信息
     */
    @RequiresPermissions("system:count:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        FceeCount fceeCount = fceeCountService.selectFceeCountById(id);
        mmap.put("fceeCount", fceeCount);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户信息
     */
    @RequiresPermissions("system:count:edit")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(FceeCount fceeCount)
    {
        return toAjax(fceeCountService.updateFceeCount(fceeCount));
    }

    /**
     * 删除用户信息
     */
    @RequiresPermissions("system:count:remove")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(fceeCountService.deleteFceeCountByIds(ids));
    }
}
