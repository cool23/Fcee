package com.ruoyi.project.system.school.service;

import com.ruoyi.project.system.school.domain.CampusScenery;

import java.util.List;
import java.util.Set;

public interface CampusSceneryService {
        public Set<CampusScenery> HomeCampusScenery(String[] schoolimgs);
        public List<CampusScenery> HomeCSP();

}
