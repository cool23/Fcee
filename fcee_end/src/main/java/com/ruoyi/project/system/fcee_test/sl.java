package com.ruoyi.project.system.fcee_test;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

@Data
public class sl {
    /** id */
    private Long sid;

    /** 院校代号 */
    @Excel(name = "院校代号")
    private String SchoolCode;

    /** 院校名称 */
    @Excel(name = "院校名称")
    private String SchoolName;

    /** 省份 */
    @Excel(name = "省份")
    private String province;

    /** 办学类型 */
    @Excel(name = "办学类型")
    private String SchoolType;

    /** 总计划数 */
    @Excel(name = "总计划数")
    private String totalNumberOfPlans;

    /** 专业代号 */
    @Excel(name = "专业代号")
    private String ProfessionalCode;

    /** 专业名称（类及备注） */
    @Excel(name = "专业名称", readConverterExp = "类=及备注")
    private String ProfessionalName;

    /** 选考科目要求 */
    @Excel(name = "选考科目要求")
    private String ElectiveSubjectRequirements;

    /** 学制 */
    @Excel(name = "学制")
    private String AcademicSystem;

    /** 计划数 */
    @Excel(name = "计划数")
    private String numberOfPlans;

    /** 年收费（元） */
    @Excel(name = "年收费", readConverterExp = "元=")
    private String annualFee;
}
