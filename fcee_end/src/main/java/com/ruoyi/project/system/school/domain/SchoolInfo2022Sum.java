package com.ruoyi.project.system.school.domain;

//import com.ruoyi.project.system.school.controller.SchoolIf;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolInfo2022Sum extends FceeSchool {

    public  String CollegeCode;
    public  String citys;
    //专业代码总
    public  String ProfessionalCodeSum;
    //专业名称类
    public String Professionalnametype;


    //专业名称
    /** 院校代号 */
    private String SchoolCode;

    /** 院校名称 */
    private String SchoolName;

    /** 省份 */
    private String province;

    /** 办学类型 */
    private String SchoolType;

    /** 总计划数 */
    private String totalNumberOfPlans;

    /** 专业代号 */

    private String ProfessionalCode;

    /** 专业名称（类及备注） */

    private String ProfessionalName;

    /** 选考科目要求 */
    private String ElectiveSubjectRequirements;

    //
    public String SubjectAssessmentFor4;

    //2021最低名
    public String TheLowestRankingIn2021;
    public String TheLowestRankingIn2020;
    //常规批次
    public String regularBatch;

    public String ProvinceCityCode;//省市代码

    public String ProvinceCityName;//省市名称

//    public String SchoolType;//办学类型

    public  String natureCategory;//性质类别

    public SchoolInfo2022Sum() {
    }

    public SchoolInfo2022Sum(String schoolCode, String professionalCode) {
        SchoolCode = schoolCode;
        ProfessionalCode = professionalCode;
    }
}
