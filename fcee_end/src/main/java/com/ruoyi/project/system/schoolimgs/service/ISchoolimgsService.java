package com.ruoyi.project.system.schoolimgs.service;

import java.util.List;
import com.ruoyi.project.system.schoolimgs.domain.Schoolimgs;

/**
 * 院校封面图片Service接口
 * 
 * @author ruoyi
 * @date 2022-07-10
 */
public interface ISchoolimgsService 
{
    /**
     * 查询院校封面图片
     * 
     * @param id 院校封面图片主键
     * @return 院校封面图片
     */
    public Schoolimgs selectSchoolimgsById(Long id);

    /**
     * 查询院校封面图片列表
     * 
     * @param schoolimgs 院校封面图片
     * @return 院校封面图片集合
     */
    public List<Schoolimgs> selectSchoolimgsList(Schoolimgs schoolimgs);

    /**
     * 新增院校封面图片
     * 
     * @param schoolimgs 院校封面图片
     * @return 结果
     */
    public int insertSchoolimgs(Schoolimgs schoolimgs);

    /**
     * 修改院校封面图片
     * 
     * @param schoolimgs 院校封面图片
     * @return 结果
     */
    public int updateSchoolimgs(Schoolimgs schoolimgs);

    /**
     * 批量删除院校封面图片
     * 
     * @param ids 需要删除的院校封面图片主键集合
     * @return 结果
     */
    public int deleteSchoolimgsByIds(String ids);

    /**
     * 删除院校封面图片信息
     * 
     * @param id 院校封面图片主键
     * @return 结果
     */
    public int deleteSchoolimgsById(Long id);
}
