package com.ruoyi.project.system.fceeschool2022.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 2022院校信息（本科）对象 fceeschool2022
 * 
 * @author 宋伟
 * @date 2022-07-15
 */
public class Fceeschool2022 extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long sid;

    /** 学校代号 */
    @Excel(name = "学校代号")
    private String CollegeCode;

    /** 院校代码 */
    @Excel(name = "院校代码")
    private String schoolCode;

    /** 院校名称 */
    @Excel(name = "院校名称")
    private String SchoolName;

    /** 省份 */
    @Excel(name = "省份")
    private String province;

    /** 城市 */
    @Excel(name = "城市")
    private String citys;

    /** 办学类型 */
    @Excel(name = "办学类型")
    private String SchoolType;

    /** 总计划数 */
    @Excel(name = "总计划数")
    private String totalNumberOfPlans;

    /** 专业代码 */
    @Excel(name = "专业代码")
    private String ProfessionalCodeSum;

    /** 专业代号 */
    @Excel(name = "专业代号")
    private String ProfessionalCode;

    /** 专业名称 */
    @Excel(name = "专业名称")
    private String ProfessionalName;

    /**  */
    @Excel(name = "")
    private String Professionalnametype;

    /** 第四次学科评价 */
    @Excel(name = "第四次学科评价")
    private String SubjectAssessmentFor4;

    /**  */
    @Excel(name = "")
    private String ElectiveSubjectRequirements;

    /**  */
    @Excel(name = "")
    private String AcademicSystem;

    /**  */
    @Excel(name = "")
    private String numberOfPlans;

    /**  */
    @Excel(name = "")
    private String annualFee;

    /** 2021年投档最低位次 */
    @Excel(name = "2021年投档最低位次")
    private String TheLowestRankingIn2021;

    /** 2020年投档最低位次 */
    @Excel(name = "2020年投档最低位次")
    private String TheLowestRankingIn2020;

    /** 常规批次 */
    @Excel(name = "常规批次")
    private String regularBatch;

    public void setSid(Long sid)
    {
        this.sid = sid;
    }

    public Long getSid()
    {
        return sid;
    }
    public void setCollegeCode(String CollegeCode)
    {
        this.CollegeCode = CollegeCode;
    }

    public String getCollegeCode()
    {
        return CollegeCode;
    }
    public void setSchoolCode(String schoolCode)
    {
        this.schoolCode = schoolCode;
    }

    public String getSchoolCode()
    {
        return schoolCode;
    }
    public void setSchoolName(String SchoolName)
    {
        this.SchoolName = SchoolName;
    }

    public String getSchoolName()
    {
        return SchoolName;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getProvince()
    {
        return province;
    }
    public void setCitys(String citys)
    {
        this.citys = citys;
    }

    public String getCitys()
    {
        return citys;
    }
    public void setSchoolType(String SchoolType)
    {
        this.SchoolType = SchoolType;
    }

    public String getSchoolType()
    {
        return SchoolType;
    }
    public void setTotalNumberOfPlans(String totalNumberOfPlans)
    {
        this.totalNumberOfPlans = totalNumberOfPlans;
    }

    public String getTotalNumberOfPlans()
    {
        return totalNumberOfPlans;
    }
    public void setProfessionalCodeSum(String ProfessionalCodeSum)
    {
        this.ProfessionalCodeSum = ProfessionalCodeSum;
    }

    public String getProfessionalCodeSum()
    {
        return ProfessionalCodeSum;
    }
    public void setProfessionalCode(String ProfessionalCode)
    {
        this.ProfessionalCode = ProfessionalCode;
    }

    public String getProfessionalCode()
    {
        return ProfessionalCode;
    }
    public void setProfessionalName(String ProfessionalName)
    {
        this.ProfessionalName = ProfessionalName;
    }

    public String getProfessionalName()
    {
        return ProfessionalName;
    }
    public void setProfessionalnametype(String Professionalnametype)
    {
        this.Professionalnametype = Professionalnametype;
    }

    public String getProfessionalnametype()
    {
        return Professionalnametype;
    }
    public void setSubjectAssessmentFor4(String SubjectAssessmentFor4)
    {
        this.SubjectAssessmentFor4 = SubjectAssessmentFor4;
    }

    public String getSubjectAssessmentFor4()
    {
        return SubjectAssessmentFor4;
    }
    public void setElectiveSubjectRequirements(String ElectiveSubjectRequirements)
    {
        this.ElectiveSubjectRequirements = ElectiveSubjectRequirements;
    }

    public String getElectiveSubjectRequirements()
    {
        return ElectiveSubjectRequirements;
    }
    public void setAcademicSystem(String AcademicSystem)
    {
        this.AcademicSystem = AcademicSystem;
    }

    public String getAcademicSystem()
    {
        return AcademicSystem;
    }
    public void setNumberOfPlans(String numberOfPlans)
    {
        this.numberOfPlans = numberOfPlans;
    }

    public String getNumberOfPlans()
    {
        return numberOfPlans;
    }
    public void setAnnualFee(String annualFee)
    {
        this.annualFee = annualFee;
    }

    public String getAnnualFee()
    {
        return annualFee;
    }
    public void setTheLowestRankingIn2021(String TheLowestRankingIn2021)
    {
        this.TheLowestRankingIn2021 = TheLowestRankingIn2021;
    }

    public String getTheLowestRankingIn2021()
    {
        return TheLowestRankingIn2021;
    }
    public void setTheLowestRankingIn2020(String TheLowestRankingIn2020)
    {
        this.TheLowestRankingIn2020 = TheLowestRankingIn2020;
    }

    public String getTheLowestRankingIn2020()
    {
        return TheLowestRankingIn2020;
    }
    public void setRegularBatch(String regularBatch)
    {
        this.regularBatch = regularBatch;
    }

    public String getRegularBatch()
    {
        return regularBatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("sid", getSid())
            .append("CollegeCode", getCollegeCode())
            .append("schoolCode", getSchoolCode())
            .append("SchoolName", getSchoolName())
            .append("province", getProvince())
            .append("citys", getCitys())
            .append("SchoolType", getSchoolType())
            .append("totalNumberOfPlans", getTotalNumberOfPlans())
            .append("ProfessionalCodeSum", getProfessionalCodeSum())
            .append("ProfessionalCode", getProfessionalCode())
            .append("ProfessionalName", getProfessionalName())
            .append("Professionalnametype", getProfessionalnametype())
            .append("SubjectAssessmentFor4", getSubjectAssessmentFor4())
            .append("ElectiveSubjectRequirements", getElectiveSubjectRequirements())
            .append("AcademicSystem", getAcademicSystem())
            .append("numberOfPlans", getNumberOfPlans())
            .append("annualFee", getAnnualFee())
            .append("TheLowestRankingIn2021", getTheLowestRankingIn2021())
            .append("TheLowestRankingIn2020", getTheLowestRankingIn2020())
            .append("regularBatch", getRegularBatch())
            .toString();
    }
}
