package com.ruoyi.project.system.user.controller;

import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.tool.fceeTool.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

//用于获取
@CrossOrigin
@RequestMapping("/userInfo")
@RestController
public class userInfo extends RegisterController {

    HashMap userMap  = new HashMap();//建立一集合储存用户信息
    @GetMapping
    public Result<HashMap> info(){//返回用户信息
//        startPage();
        User currentUser = ShiroUtils.getSysUser();
        String Avatar =   currentUser.getAvatar();
        long UserId = currentUser.getUserId();
        String username = currentUser.getUserName();
        userMap.put("UserId",UserId);
        userMap.put("Avatar",Avatar);
        userMap.put("username",username);
//        userList.add(username);
//        userList.add(Avatar);
//        arr.addAll(userList);
        return new Result<HashMap>(0,userMap,"ok");
    }

    @Override
    @PostMapping("/Register")
    public AjaxResult ajaxRegister(User user) {
        return super.ajaxRegister(user);
    }
}
