package com.ruoyi.project.system.school.service.impl;

import com.ruoyi.project.system.school.domain.SchoolInfo2022Sum;
import com.ruoyi.project.system.school.mapper.SchoolInfoSum;
import com.ruoyi.project.system.school.service.SchoolinfoSumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SchoolinfoSumServiceImpl implements SchoolinfoSumService {
    @Autowired
    SchoolInfoSum schoolInfoSum;



    @Override
    public List<SchoolInfo2022Sum> ListByInfo(SchoolInfo2022Sum schoolInfo2022Sum) {
        return null;
    }

    @Override
    public List<SchoolInfo2022Sum> getSchoolAll() {
        return schoolInfoSum.getSchoolAll();
    }

    @Override
    public List<SchoolInfo2022Sum> SeachSchool(SchoolInfo2022Sum schoolInfo2022Sum) {
        return schoolInfoSum.SeachSchool(schoolInfo2022Sum);
    }


    @Override
    public List<SchoolInfo2022Sum> School_infoAll(SchoolInfo2022Sum schoolInfo2022Sum) {
        List info_all = new ArrayList<SchoolInfo2022Sum>();
        info_all.addAll(schoolInfoSum.School_info_list(schoolInfo2022Sum));
        info_all.addAll(schoolInfoSum.School_info_listSpecialist(schoolInfo2022Sum));

        return info_all;
    }

    public Set<SchoolInfo2022Sum> rank_infoAll(SchoolInfo2022Sum schoolInfo2022Sum) {
        Set<SchoolInfo2022Sum> info_all = new HashSet<>();
        info_all.addAll(schoolInfoSum.simulation_info_list(schoolInfo2022Sum));
//        info_all.addAll((Collection) schoolInfoSum.simulation_info_listSpecialist(schoolInfo2022Sum));

        return info_all;
    }
}
