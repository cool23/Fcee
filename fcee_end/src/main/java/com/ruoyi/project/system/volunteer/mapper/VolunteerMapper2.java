package com.ruoyi.project.system.volunteer.mapper;

import com.ruoyi.project.system.volunteer.domain.FilingPlan;
import com.ruoyi.project.system.volunteer.domain.Piecebypiece;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface VolunteerMapper2 {

    /*获取分数段*/
    @Select("select * from piecebypiece where Score = #{Score}")
    public Piecebypiece getListScore (Piecebypiece piecebypiece);

    /*模拟一次*/
    @Select("select * from onevolunteer where #{LowNumberPlans}<= LowNumberPlans ")
    public  List<FilingPlan> Once(FilingPlan filingPlan);

    /*模拟二次*/
    @Select("select * from twovolunteer where  #{LowNumberPlans}<= LowNumberPlans ")
    public  List<FilingPlan> Two(FilingPlan filingPlan);

    /*模拟二次*/
    @Select("select * from threevolunteer where #{LowNumberPlans} <= LowNumberPlans  ")
    public  List<FilingPlan> Three(FilingPlan filingPlan);

}
