package com.ruoyi.project.system.fceeUser.service.impl;

import com.ruoyi.project.system.fceeUser.domain.FceeUser;
import com.ruoyi.project.system.fceeUser.mapper.FceeUserMapper;
import com.ruoyi.project.system.fceeUser.service.IFceeUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//@Service
public class FceeUserService implements IFceeUserService {
    @Autowired
    FceeUserMapper fceeUserMapper;
    @Override
    public FceeUser Flongin(String count, String passwd) {
        return fceeUserMapper.Flongin(count,passwd);
    }

    @Override
    public int FRegister(FceeUser fceeUser) {
        return fceeUserMapper.FRegister(fceeUser);
    }
}
