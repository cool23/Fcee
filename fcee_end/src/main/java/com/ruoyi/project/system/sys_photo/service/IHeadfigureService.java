package com.ruoyi.project.system.sys_photo.service;

import java.util.List;
import com.ruoyi.project.system.sys_photo.domain.Headfigure;

/**
 * 院校图片Service接口
 * 
 * @author ruoyi
 * @date 2022-07-08
 */
public interface IHeadfigureService 
{
    /**
     * 查询院校图片
     * 
     * @param id 院校图片主键
     * @return 院校图片
     */
    public Headfigure selectHeadfigureById(Long id);

    /**
     * 查询院校图片列表
     * 
     * @param headfigure 院校图片
     * @return 院校图片集合
     */
    public List<Headfigure> selectHeadfigureList(Headfigure headfigure);

    /**
     * 新增院校图片
     * 
     * @param headfigure 院校图片
     * @return 结果
     */
    public int insertHeadfigure(Headfigure headfigure);

    /**
     * 修改院校图片
     * 
     * @param headfigure 院校图片
     * @return 结果
     */
    public int updateHeadfigure(Headfigure headfigure);

    /**
     * 批量删除院校图片
     * 
     * @param ids 需要删除的院校图片主键集合
     * @return 结果
     */
    public int deleteHeadfigureByIds(String ids);

    /**
     * 删除院校图片信息
     * 
     * @param id 院校图片主键
     * @return 结果
     */
    public int deleteHeadfigureById(Long id);
}
