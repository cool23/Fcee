package com.ruoyi.project.system.sys_photo.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.sys_photo.domain.Headfigure;
import com.ruoyi.project.system.sys_photo.service.IHeadfigureService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 院校图片Controller
 * 
 * @author ruoyi
 * @date 2022-07-08
 */
@Controller
@RequestMapping("/system/sys_photo")
public class HeadfigureController extends BaseController
{
    private String prefix = "system/sys_photo";

    @Autowired
    private IHeadfigureService headfigureService;

    @RequiresPermissions("system:sys_photo:view")
    @GetMapping()
    public String sys_photo()
    {
        return prefix + "/sys_photo";
    }


    @RequiresPermissions("system:sys_photo:view")
    @GetMapping("/ps2")
    public String sys_photo2()
    {
        System.out.println("ps2");
        return prefix + "/sys_photoup.html";
    }

    /**
     * 查询院校图片列表
     */
    @RequiresPermissions("system:sys_photo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Headfigure headfigure)
    {
        startPage();
        List<Headfigure> list = headfigureService.selectHeadfigureList(headfigure);
        return getDataTable(list);
    }

    /**
     * 导出院校图片列表
     */
    @RequiresPermissions("system:sys_photo:export")
    @Log(title = "院校图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Headfigure headfigure)
    {
        List<Headfigure> list = headfigureService.selectHeadfigureList(headfigure);
        ExcelUtil<Headfigure> util = new ExcelUtil<Headfigure>(Headfigure.class);
        return util.exportExcel(list, "院校图片数据");
    }

    /**
     * 新增院校图片
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存院校图片
     */
    @RequiresPermissions("system:sys_photo:add")
    @Log(title = "院校图片", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Headfigure headfigure)
    {
        return toAjax(headfigureService.insertHeadfigure(headfigure));
    }

    /**
     * 修改院校图片
     */
    @RequiresPermissions("system:sys_photo:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Headfigure headfigure = headfigureService.selectHeadfigureById(id);
        mmap.put("headfigure", headfigure);
        return prefix + "/edit";
    }

    /**
     * 修改保存院校图片
     */
    @RequiresPermissions("system:sys_photo:edit")
    @Log(title = "院校图片", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Headfigure headfigure)
    {
        return toAjax(headfigureService.updateHeadfigure(headfigure));
    }

    /**
     * 删除院校图片
     */
    @RequiresPermissions("system:sys_photo:remove")
    @Log(title = "院校图片", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(headfigureService.deleteHeadfigureByIds(ids));
    }
}
