package com.ruoyi.project.system.HeadFigure.mapper;

import com.ruoyi.project.system.HeadFigure.domain.HP;
import org.apache.ibatis.annotations.*;

import java.util.List;


public interface hphotosMapper {

    //添加

//    INSERT INTO `fcee`.`headfigure` (`newFileName`, `url`, `fileName`, `originalFilename`) VALUES ('124e', '21f', '2ed', 'vcdf')
//    @Insert("insert into headfigure values(null,url=#{url},fileName=#{fileName})")
    public int save(HP hp);
    //修改
    @Update("update headfigure set newFileName=#{newFileName},url=#{url},originalFilename=#{originalFilename}")
    public int update(HP hp);
    //删除
    @Delete("delete from headfigure where id=#{id}")
    public int del(int id);
    //查询
    @Select("select * from headfigure")
    public List<HP> getall();

    @Select("select * from headfigure where id=#{id}")
    public HP getByid(int id);


}
