package com.ruoyi.project.system.fceeschool2022.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.fceeschool2022.mapper.Fceeschool2022Mapper;
import com.ruoyi.project.system.fceeschool2022.domain.Fceeschool2022;
import com.ruoyi.project.system.fceeschool2022.service.IFceeschool2022Service;
import com.ruoyi.common.utils.text.Convert;

/**
 * 2022院校信息（本科）Service业务层处理
 * 
 * @author 宋伟
 * @date 2022-07-15
 */
@Service
public class Fceeschool2022ServiceImpl implements IFceeschool2022Service 
{
    @Autowired
    private Fceeschool2022Mapper fceeschool2022Mapper;

    /**
     * 查询2022院校信息（本科）
     * 
     * @param sid 2022院校信息（本科）主键
     * @return 2022院校信息（本科）
     */
    @Override
    public Fceeschool2022 selectFceeschool2022BySid(Long sid)
    {
        return fceeschool2022Mapper.selectFceeschool2022BySid(sid);
    }

    /**
     * 查询2022院校信息（本科）列表
     * 
     * @param fceeschool2022 2022院校信息（本科）
     * @return 2022院校信息（本科）
     */
    @Override
    public List<Fceeschool2022> selectFceeschool2022List(Fceeschool2022 fceeschool2022)
    {
        return fceeschool2022Mapper.selectFceeschool2022List(fceeschool2022);
    }

    /**
     * 新增2022院校信息（本科）
     * 
     * @param fceeschool2022 2022院校信息（本科）
     * @return 结果
     */
    @Override
    public int insertFceeschool2022(Fceeschool2022 fceeschool2022)
    {
        return fceeschool2022Mapper.insertFceeschool2022(fceeschool2022);
    }

    /**
     * 修改2022院校信息（本科）
     * 
     * @param fceeschool2022 2022院校信息（本科）
     * @return 结果
     */
    @Override
    public int updateFceeschool2022(Fceeschool2022 fceeschool2022)
    {
        return fceeschool2022Mapper.updateFceeschool2022(fceeschool2022);
    }

    /**
     * 批量删除2022院校信息（本科）
     * 
     * @param sids 需要删除的2022院校信息（本科）主键
     * @return 结果
     */
    @Override
    public int deleteFceeschool2022BySids(String sids)
    {
        return fceeschool2022Mapper.deleteFceeschool2022BySids(Convert.toStrArray(sids));
    }

    /**
     * 删除2022院校信息（本科）信息
     * 
     * @param sid 2022院校信息（本科）主键
     * @return 结果
     */
    @Override
    public int deleteFceeschool2022BySid(Long sid)
    {
        return fceeschool2022Mapper.deleteFceeschool2022BySid(sid);
    }
}
