package com.ruoyi.project.system.count.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.count.mapper.FceeCountMapper;
import com.ruoyi.project.system.count.domain.FceeCount;
import com.ruoyi.project.system.count.service.IFceeCountService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户信息Service业务层处理
 * 
 * @author songwei
 * @date 2022-06-09
 */
@Service
public class FceeCountServiceImpl implements IFceeCountService 
{
    @Autowired
    private FceeCountMapper fceeCountMapper;

    /**
     * 查询用户信息
     * 
     * @param id 用户信息主键
     * @return 用户信息
     */
    @Override
    public FceeCount selectFceeCountById(Long id)
    {
        return fceeCountMapper.selectFceeCountById(id);
    }

    /**
     * 查询用户信息列表
     * 
     * @param fceeCount 用户信息
     * @return 用户信息
     */
    @Override
    public List<FceeCount> selectFceeCountList(FceeCount fceeCount)
    {
        return fceeCountMapper.selectFceeCountList(fceeCount);
    }

    /**
     * 新增用户信息
     * 
     * @param fceeCount 用户信息
     * @return 结果
     */
    @Override
    public int insertFceeCount(FceeCount fceeCount)
    {
        return fceeCountMapper.insertFceeCount(fceeCount);
    }

    /**
     * 修改用户信息
     * 
     * @param fceeCount 用户信息
     * @return 结果
     */
    @Override
    public int updateFceeCount(FceeCount fceeCount)
    {
        return fceeCountMapper.updateFceeCount(fceeCount);
    }

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的用户信息主键
     * @return 结果
     */
    @Override
    public int deleteFceeCountByIds(String ids)
    {
        return fceeCountMapper.deleteFceeCountByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户信息信息
     * 
     * @param id 用户信息主键
     * @return 结果
     */
    @Override
    public int deleteFceeCountById(Long id)
    {
        return fceeCountMapper.deleteFceeCountById(id);
    }
}
