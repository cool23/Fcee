package com.ruoyi.project.system.school.mapper;

import com.ruoyi.project.system.school.domain.Specialized;
import com.ruoyi.project.system.school.domain.SpecializedList;
import com.ruoyi.project.system.school.domain.SpecializedPages;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface specializedMapper {
    @Select("select distinct * from fceeschool2022")
    public List<Specialized> getAll();

    @Select("select distinct * from fceeschoolspecialist2022 ")
    public List<Specialized> getAllspecialized();

    @Select("select  * from fceeschool2022,fceeschoolspecialist2022 where ProfessionalName like '%${ProfessionalName}%'")
    public List<Specialized> Seachspecialized(String ProfessionalName);

//    fceeschoolspecialist2022
    @Select("select distinct * from fceeschool2022 where SchoolCode like '%${SchoolCode}%'" +
            "and ProfessionalCode like '%${ProfessionalCode}%'" +
            "and ProfessionalName like '%${ProfessionalName}%'")
    public List<Specialized> Seachspecialized_Sp(Specialized specialized);

    @Select("select distinct * from fceeschoolspecialist2022 where SchoolCode like '%${SchoolCode}%'" +
            "and ProfessionalCode like '%${ProfessionalCode}%'" +
            "and ProfessionalName like '%${ProfessionalName}%'")
    public List<Specialized> Seachspecialized_ed(Specialized Specialized);//专科


//    public List<SpecializedPages> Category();

    @Select("select * from specialized where Category like '%${Category}%' and  Professional like '%${Professional}%' group by Category,Professional")
    public  List<SpecializedPages> SpecializedHomes(SpecializedPages specializedPages);
    @Select("select * from specialized where Category like '%${Category}%' and Professional like '%${Professional}%' " +
            "and professionaltitle like '%${professionaltitle}%' ")
    public  List<SpecializedList> SpecializedHomesname(SpecializedList specializedList);


}
