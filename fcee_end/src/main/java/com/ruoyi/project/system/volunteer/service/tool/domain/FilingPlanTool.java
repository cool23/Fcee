package com.ruoyi.project.system.volunteer.service.tool.domain;

import lombok.Data;

@Data
public class FilingPlanTool {
    private Integer id;//id
    private  String SpecializednName;//专业及名称
    private String SpecializednCode;//专业代号
    private  String SchoolName;//院校名称
    private  String SchoolCode;//学习代号
    private  int NumberPlans;//投档计划数
    private  Long LowNumberPlans;//投档最低位次
    private  double lowNumberScore;//投档最低分（综合分）
}
