package com.ruoyi.project.system.HeadFigure.service.impl;

import com.ruoyi.project.system.HeadFigure.domain.HP;
import com.ruoyi.project.system.HeadFigure.mapper.hphotosMapper;
import com.ruoyi.project.system.HeadFigure.service.hphotosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class hphotosServiceimpl implements hphotosService {
    @Autowired
    hphotosMapper hm;

    @Override
    public boolean save(HP hp) {
        return hm.save(hp)>0;
    }

    @Override
    public boolean update(HP hp) {
        return hm.update(hp)>0;
    }

    @Override
    public boolean del(int id) {
        return hm.del(id)>0;
    }

    @Override
    public List<HP> getall() {
        return hm.getall();
    }

    @Override
    public HP getByid(int id) {
        return hm.getByid(id);
    }
}
