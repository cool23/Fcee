package com.ruoyi.project.system.school.domain;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestMapping;

//校园风光实体类
@Data

public class CampusScenery {
    //院校名称
    private String SchoolName;
    //地址
    private String url;
}
