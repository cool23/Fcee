package com.ruoyi.project.system.school.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.school.domain.FceeSchool;
import lombok.Data;

@Data
public class School_info {

    /** 院校代号 */
    private String SchoolCode;

    /** 院校名称 */
    private String SchoolName;

    /** 省份 */
    private String province;

    /** 办学类型 */
    private String SchoolType;

    /** 总计划数 */
    private String totalNumberOfPlans;

    /** 专业代号 */

    private String ProfessionalCode;

    /** 专业名称（类及备注） */

    private String ProfessionalName;

    /** 选考科目要求 */
    private String ElectiveSubjectRequirements;


    private int pageNum;//页数
    private int pageSize;

//    private  int PageSize;//一共多少页




}
