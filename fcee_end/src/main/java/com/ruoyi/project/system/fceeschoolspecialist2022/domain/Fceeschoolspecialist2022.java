package com.ruoyi.project.system.fceeschoolspecialist2022.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 fceeschoolspecialist2022
 * 
 * @author ruoyi
 * @date 2022-07-15
 */
public class Fceeschoolspecialist2022 extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long sid;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String SchoolCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String SchoolName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String province;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String SchoolType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String totalNumberOfPlans;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String ProfessionalCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String ProfessionalName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String ElectiveSubjectRequirements;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String AcademicSystem;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String numberOfPlans;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String annualFee;

    public void setSid(Long sid)
    {
        this.sid = sid;
    }

    public Long getSid()
    {
        return sid;
    }
    public void setSchoolCode(String SchoolCode)
    {
        this.SchoolCode = SchoolCode;
    }

    public String getSchoolCode()
    {
        return SchoolCode;
    }
    public void setSchoolName(String SchoolName)
    {
        this.SchoolName = SchoolName;
    }

    public String getSchoolName()
    {
        return SchoolName;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getProvince()
    {
        return province;
    }
    public void setSchoolType(String SchoolType)
    {
        this.SchoolType = SchoolType;
    }

    public String getSchoolType()
    {
        return SchoolType;
    }
    public void setTotalNumberOfPlans(String totalNumberOfPlans)
    {
        this.totalNumberOfPlans = totalNumberOfPlans;
    }

    public String getTotalNumberOfPlans()
    {
        return totalNumberOfPlans;
    }
    public void setProfessionalCode(String ProfessionalCode)
    {
        this.ProfessionalCode = ProfessionalCode;
    }

    public String getProfessionalCode()
    {
        return ProfessionalCode;
    }
    public void setProfessionalName(String ProfessionalName)
    {
        this.ProfessionalName = ProfessionalName;
    }

    public String getProfessionalName()
    {
        return ProfessionalName;
    }
    public void setElectiveSubjectRequirements(String ElectiveSubjectRequirements)
    {
        this.ElectiveSubjectRequirements = ElectiveSubjectRequirements;
    }

    public String getElectiveSubjectRequirements()
    {
        return ElectiveSubjectRequirements;
    }
    public void setAcademicSystem(String AcademicSystem)
    {
        this.AcademicSystem = AcademicSystem;
    }

    public String getAcademicSystem()
    {
        return AcademicSystem;
    }
    public void setNumberOfPlans(String numberOfPlans)
    {
        this.numberOfPlans = numberOfPlans;
    }

    public String getNumberOfPlans()
    {
        return numberOfPlans;
    }
    public void setAnnualFee(String annualFee)
    {
        this.annualFee = annualFee;
    }

    public String getAnnualFee()
    {
        return annualFee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("sid", getSid())
            .append("SchoolCode", getSchoolCode())
            .append("SchoolName", getSchoolName())
            .append("province", getProvince())
            .append("SchoolType", getSchoolType())
            .append("totalNumberOfPlans", getTotalNumberOfPlans())
            .append("ProfessionalCode", getProfessionalCode())
            .append("ProfessionalName", getProfessionalName())
            .append("ElectiveSubjectRequirements", getElectiveSubjectRequirements())
            .append("AcademicSystem", getAcademicSystem())
            .append("numberOfPlans", getNumberOfPlans())
            .append("annualFee", getAnnualFee())
            .toString();
    }
}
