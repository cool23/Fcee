package com.ruoyi.project.system.fceeschoolspecialist2022.service;

import java.util.List;
import com.ruoyi.project.system.fceeschoolspecialist2022.domain.Fceeschoolspecialist2022;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2022-07-15
 */
public interface IFceeschoolspecialist2022Service 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param sid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Fceeschoolspecialist2022 selectFceeschoolspecialist2022BySid(Long sid);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param fceeschoolspecialist2022 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Fceeschoolspecialist2022> selectFceeschoolspecialist2022List(Fceeschoolspecialist2022 fceeschoolspecialist2022);

    /**
     * 新增【请填写功能名称】
     * 
     * @param fceeschoolspecialist2022 【请填写功能名称】
     * @return 结果
     */
    public int insertFceeschoolspecialist2022(Fceeschoolspecialist2022 fceeschoolspecialist2022);

    /**
     * 修改【请填写功能名称】
     * 
     * @param fceeschoolspecialist2022 【请填写功能名称】
     * @return 结果
     */
    public int updateFceeschoolspecialist2022(Fceeschoolspecialist2022 fceeschoolspecialist2022);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param sids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteFceeschoolspecialist2022BySids(String sids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param sid 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteFceeschoolspecialist2022BySid(Long sid);
}
