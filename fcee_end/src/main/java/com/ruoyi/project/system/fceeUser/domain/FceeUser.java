package com.ruoyi.project.system.fceeUser.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Data

public class FceeUser {
    private String user_name;
    private String count;
    private  String passwd;
    private Integer score;
}
