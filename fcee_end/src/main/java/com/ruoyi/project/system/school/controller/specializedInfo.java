package com.ruoyi.project.system.school.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.school.domain.Specialized;
import com.ruoyi.project.system.school.domain.SpecializedList;
import com.ruoyi.project.system.school.domain.SpecializedPages;
import com.ruoyi.project.system.school.service.specializedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/specialized")
@Controller
public class specializedInfo extends BaseController {
    //type01 全部 02本科 03专科
    @Autowired
    specializedService specializedService;

    @GetMapping("/getspecializedAll/{type}")//获取全部专业
    public TableDataInfo getspecializedInfoAll(@PathVariable int type){
        startPage();
        return getDataTable(specializedService.getAll(type));
    }

    @GetMapping ("/Sceachspecialized")//获取全部专业
    @ResponseBody
    public TableDataInfo Seachspecializedinfo(Specialized specialized){
        startPage();
        List<Specialized> sp = specializedService.Seachspecialized_Sp(specialized);


        return getDataTable(sp);
    }

    @GetMapping("/getSpecializedPages")
    @ResponseBody
    public  TableDataInfo getSpecialized(SpecializedPages specializedPages){
        startPage();
        List<SpecializedPages> spages = specializedService.SpecializedHomes(specializedPages);
        return getDataTable(spages);
    }

    @GetMapping("/getSpecializedName")
    @ResponseBody
    public  TableDataInfo getSpecializedName(SpecializedList specializedList){
        startPage();
        List<SpecializedList> list = specializedService.SpecializedHomesname(specializedList);

        return getDataTable(list);
    }

}
