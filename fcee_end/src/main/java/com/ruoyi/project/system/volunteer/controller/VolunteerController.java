package com.ruoyi.project.system.volunteer.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.volunteer.domain.Volunteer;
import com.ruoyi.project.system.volunteer.service.IVolunteerService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 我的志愿单Controller
 * 
 * @author sw
 * @date 2022-08-25
 */
@Controller
@RequestMapping("/system/volunteer")
public class VolunteerController extends BaseController
{
    private String prefix = "system/volunteer";

    @Autowired
    private IVolunteerService volunteerService;

    @RequiresPermissions("system:volunteer:view")
    @GetMapping()
    public String volunteer()
    {
        return prefix + "/volunteer";
    }

    /**
     * 查询我的志愿单列表
     */
    @RequiresPermissions("system:volunteer:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Volunteer volunteer)
    {
        startPage();
        List<Volunteer> list = volunteerService.selectVolunteerList(volunteer);
        return getDataTable(list);
    }

    /**
     * 导出我的志愿单列表
     */
    @RequiresPermissions("system:volunteer:export")
    @Log(title = "我的志愿单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Volunteer volunteer)
    {
        List<Volunteer> list = volunteerService.selectVolunteerList(volunteer);
        ExcelUtil<Volunteer> util = new ExcelUtil<Volunteer>(Volunteer.class);
        return util.exportExcel(list, "我的志愿单数据");
    }

    /**
     * 新增我的志愿单
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存我的志愿单
     */
    @RequiresPermissions("system:volunteer:add")
    @Log(title = "我的志愿单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Volunteer volunteer)
    {
        return toAjax(volunteerService.insertVolunteer(volunteer));
    }

    /**
     * 修改我的志愿单
     */
    @RequiresPermissions("system:volunteer:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Volunteer volunteer = volunteerService.selectVolunteerById(id);
        mmap.put("volunteer", volunteer);
        return prefix + "/edit";
    }

    /**
     * 修改保存我的志愿单
     */
    @RequiresPermissions("system:volunteer:edit")
    @Log(title = "我的志愿单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Volunteer volunteer)
    {
        return toAjax(volunteerService.updateVolunteer(volunteer));
    }

    /**
     * 删除我的志愿单
     */
    @RequiresPermissions("system:volunteer:remove")
    @Log(title = "我的志愿单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(volunteerService.deleteVolunteerByIds(ids));
    }
}
