package com.ruoyi.project.system.school.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Data
public class Specialized {
    private Integer sid;
    private String SchoolCode;//开设院校代号
    private String SchoolName;//开设院校名称
    public  String professionalCodeSum;//专业代码
    private   String  ProfessionalCode;//专业代号
    private String ProfessionalName;//专业名称
    public  String specializedType;//专业类型
    private int pageNum;//页数
    private int pageSize;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Specialized)) return false;

        Specialized that = (Specialized) o;

        return new EqualsBuilder().append(getPageNum(), that.getPageNum()).append(getSid(), that.getSid()).append(getSchoolCode(), that.getSchoolCode()).append(getSchoolName(), that.getSchoolName()).append(getProfessionalCodeSum(), that.getProfessionalCodeSum()).append(getProfessionalCode(), that.getProfessionalCode()).append(getProfessionalName(), that.getProfessionalName()).append(getSpecializedType(), that.getSpecializedType()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getSid()).append(getSchoolCode()).append(getSchoolName()).append(getProfessionalCodeSum()).append(getProfessionalCode()).append(getProfessionalName()).append(getSpecializedType()).append(getPageNum()).toHashCode();
    }
}
