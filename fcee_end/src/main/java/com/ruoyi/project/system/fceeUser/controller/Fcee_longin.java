package com.ruoyi.project.system.fceeUser.controller;

import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.system.count.domain.FceeCount;
import com.ruoyi.project.system.count.service.IFceeCountService;
import com.ruoyi.project.system.fceeUser.domain.FceeUser;
import com.ruoyi.project.system.fceeUser.service.IFceeUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import java.util.List;
//
//@Api("fcee用户注册信息")
//@Controller //装配
//@RequestMapping("/api")
public class Fcee_longin extends BaseController {



    @Autowired
    public IFceeUserService FceeUserService;//引用CountService接口并自动装配

    @PostMapping("/FLongin")
//    @RequestMapping(value = "/FLongin",method = {RequestMethod.POST})
    @ApiOperation("登录")
   @ResponseBody
    public AjaxResult FLongin(String count, String passwd) {
        //count账号paswswd密码
        try {
            FceeUser fu = FceeUserService.Flongin(count,passwd);
            if (fu.getCount().equals(count)||fu.getPasswd().equals(passwd)){//校验用户是否不为空
                System.out.println(fu.getUser_name());
                return new AjaxResult( AjaxResult.Type.SUCCESS,fu.getUser_name());
            }
            else {
                return AjaxResult.error("用户名或密码错误");

            }
        }
        catch (java.lang.NullPointerException e){
            return AjaxResult.warn("数据未在数据库中");

        }

    }
    /**
     * 新增保存用户信息
     */
    @ApiOperation("注册")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping(value = "/FRegister")
    @ResponseBody
    public AjaxResult addSave(FceeUser fceeUser)  {
            if (fceeUser.getUser_name()!=null&&fceeUser.getCount()!=null&&fceeUser.getPasswd()!=null){
                return toAjax(FceeUserService.FRegister(fceeUser));
            }
            else {
                return AjaxResult.error("信息不完整");
            }

    }
}