package com.ruoyi.project.system.fceeschoolspecialist2022.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.fceeschoolspecialist2022.mapper.Fceeschoolspecialist2022Mapper;
import com.ruoyi.project.system.fceeschoolspecialist2022.domain.Fceeschoolspecialist2022;
import com.ruoyi.project.system.fceeschoolspecialist2022.service.IFceeschoolspecialist2022Service;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-15
 */
@Service
public class Fceeschoolspecialist2022ServiceImpl implements IFceeschoolspecialist2022Service 
{
    @Autowired
    private Fceeschoolspecialist2022Mapper fceeschoolspecialist2022Mapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param sid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Fceeschoolspecialist2022 selectFceeschoolspecialist2022BySid(Long sid)
    {
        return fceeschoolspecialist2022Mapper.selectFceeschoolspecialist2022BySid(sid);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param fceeschoolspecialist2022 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Fceeschoolspecialist2022> selectFceeschoolspecialist2022List(Fceeschoolspecialist2022 fceeschoolspecialist2022)
    {
        return fceeschoolspecialist2022Mapper.selectFceeschoolspecialist2022List(fceeschoolspecialist2022);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param fceeschoolspecialist2022 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertFceeschoolspecialist2022(Fceeschoolspecialist2022 fceeschoolspecialist2022)
    {
        return fceeschoolspecialist2022Mapper.insertFceeschoolspecialist2022(fceeschoolspecialist2022);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param fceeschoolspecialist2022 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateFceeschoolspecialist2022(Fceeschoolspecialist2022 fceeschoolspecialist2022)
    {
        return fceeschoolspecialist2022Mapper.updateFceeschoolspecialist2022(fceeschoolspecialist2022);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param sids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteFceeschoolspecialist2022BySids(String sids)
    {
        return fceeschoolspecialist2022Mapper.deleteFceeschoolspecialist2022BySids(Convert.toStrArray(sids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param sid 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteFceeschoolspecialist2022BySid(Long sid)
    {
        return fceeschoolspecialist2022Mapper.deleteFceeschoolspecialist2022BySid(sid);
    }
}
