package com.ruoyi.project.system.schoolimgs.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 院校封面图片对象 schoolimgs
 * 
 * @author ruoyi
 * @date 2022-07-10
 */
public class Schoolimgs extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 院校代码 */
    @Excel(name = "院校代码")
    private String SchoolCode;

    /** 地址 */
    @Excel(name = "地址")
    private String url;

    /** 文件名 */
    @Excel(name = "文件名")
    private String name;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSchoolCode(String SchoolCode)
    {
        this.SchoolCode = SchoolCode;
    }

    public String getSchoolCode()
    {
        return SchoolCode;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrl()
    {
        return url;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("SchoolCode", getSchoolCode())
            .append("url", getUrl())
            .append("name", getName())
            .toString();
    }
}
