package com.ruoyi.project.system.school.service.impl;

import com.ruoyi.project.system.school.domain.Specialized;
import com.ruoyi.project.system.school.domain.SpecializedList;
import com.ruoyi.project.system.school.domain.SpecializedPages;
import com.ruoyi.project.system.school.mapper.specializedMapper;
import com.ruoyi.project.system.school.service.specializedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class specializedServiceimpl implements specializedService {
    @Autowired
    specializedMapper specializedMapper;

    @Override
    public List<Specialized> getAll(int type) {
        //type01 全部 02本科 03专科
        List<Specialized> all = new LinkedList<Specialized>();

        if (type!=0){
            if (type==01){
                all.addAll(specializedMapper.getAll());
                all.addAll(specializedMapper.getAllspecialized());
             return all;
            }
            else if (type==02){
                return specializedMapper.getAll();
            }
            else if (type==03){
                return specializedMapper.getAllspecialized();
            }
        }
        return null;
    }

    @Override
    public List<Specialized> Seachspecialized_Sp(Specialized specialized) {
        List<Specialized> all = new ArrayList<>();


        all.addAll(specializedMapper.Seachspecialized_Sp(specialized));
        all.addAll(specializedMapper.Seachspecialized_ed(specialized));
//            all.addAll(sp_all);
            return all;
    }

    @Override
    public List<SpecializedPages> SpecializedHomes(SpecializedPages specializedPages) {
        return specializedMapper.SpecializedHomes(specializedPages);
    }

    @Override
    public List<SpecializedList> SpecializedHomesname(SpecializedList specializedList) {
        return specializedMapper.SpecializedHomesname(specializedList);
    }
}
