package com.ruoyi.project.system.volunteer.service.tool;

import com.ruoyi.project.system.volunteer.domain.FilingPlan;
import com.ruoyi.project.system.volunteer.service.tool.domain.FilingPlanTool;

import java.util.List;

//用于数据清洗整理
public interface SchoolTool {
    /*分离学校专业*/
    public FilingPlanTool SplitSchoolSpecialized(FilingPlan filingPlans);

}
