package com.ruoyi.project.system.volunteer.service.impl;

import com.ruoyi.project.system.school.service.SchoolinfoSumService;
import com.ruoyi.project.system.volunteer.domain.FilingPlan;
import com.ruoyi.project.system.volunteer.domain.Piecebypiece;
import com.ruoyi.project.system.volunteer.mapper.VolunteerMapper2;
import com.ruoyi.project.system.volunteer.service.IVolunteerService2;
import com.ruoyi.project.system.volunteer.service.tool.SchoolTool;
import com.ruoyi.project.system.volunteer.service.tool.domain.FilingPlanTool;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class VolunteerService2Impl implements IVolunteerService2, SchoolTool {
    @Autowired
    private VolunteerMapper2 volunteerMapper2;




    @Override
    public Piecebypiece getListScore(Piecebypiece piecebypiece) {
        return volunteerMapper2.getListScore(piecebypiece);
    }

    @Override
    public List<FilingPlanTool> simulation(FilingPlan filingPlan) {
//        LowNumberPlans;//投档最低位次//sp是否专科
//        Long LowNumberPlans = filingPlan.getLowNumberPlans();
        List<FilingPlanTool> simulationList = new LinkedList<FilingPlanTool>();

            List<FilingPlan> One = volunteerMapper2.Once(filingPlan);
            for (int i=0;i<One.size();i++){
                simulationList.add(SplitSchoolSpecialized(One.get(i)));//将处理结果重新添加
            }


            //第二次
            List<FilingPlan> Two = volunteerMapper2.Two(filingPlan);
            for (int i=0;i< Two.size();i++){
                simulationList.add(SplitSchoolSpecialized(Two.get(i)));
            }
            List<FilingPlan> Three = volunteerMapper2.Three(filingPlan);
            for (int i=0;i< Three.size();i++){
                simulationList.add(SplitSchoolSpecialized(Three.get(i)));
            }


        return simulationList;
    }


    @Override
    public FilingPlanTool SplitSchoolSpecialized(FilingPlan filingPlans) {
        FilingPlanTool filingPlanTool = new FilingPlanTool();
        String str = filingPlans.getSchoolCodeName();//获取院校代号名称
        filingPlanTool.setSchoolCode(str.substring(0,4));//保存分离后的代码
        filingPlanTool.setSchoolName(str.substring(4));
        //分离专业
        String sstr = filingPlans.getSpecializednName();
        filingPlanTool.setSpecializednCode(sstr.substring(0,2));
        filingPlanTool.setSpecializednName(sstr.substring(4));

        //重新赋值
        filingPlanTool.setLowNumberPlans(filingPlans.getLowNumberPlans());
        filingPlanTool.setLowNumberScore(filingPlans.getLowNumberScore());
        filingPlanTool.setNumberPlans(filingPlans.getNumberPlans());
        filingPlanTool.setId(filingPlanTool.getId());
        return filingPlanTool;

    }
}
