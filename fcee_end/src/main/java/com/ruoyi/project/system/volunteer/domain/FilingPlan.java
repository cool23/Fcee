package com.ruoyi.project.system.volunteer.domain;

import lombok.Data;

@Data
public class FilingPlan {
    private Integer id;//id
    private  String SpecializednName;//专业代号及名称
    private  String SchoolCodeName;//院校代号及名称
    private  int NumberPlans;//投档计划数
    private  Long LowNumberPlans;//投档最低位次
    private  double lowNumberScore;//投档最低分（综合分）
}
