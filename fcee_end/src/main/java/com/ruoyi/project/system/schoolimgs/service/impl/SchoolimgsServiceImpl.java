package com.ruoyi.project.system.schoolimgs.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.schoolimgs.mapper.SchoolimgsMapper;
import com.ruoyi.project.system.schoolimgs.domain.Schoolimgs;
import com.ruoyi.project.system.schoolimgs.service.ISchoolimgsService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 院校封面图片Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-10
 */
@Service
public class SchoolimgsServiceImpl implements ISchoolimgsService 
{
    @Autowired
    private SchoolimgsMapper schoolimgsMapper;

    /**
     * 查询院校封面图片
     * 
     * @param id 院校封面图片主键
     * @return 院校封面图片
     */
    @Override
    public Schoolimgs selectSchoolimgsById(Long id)
    {
        return schoolimgsMapper.selectSchoolimgsById(id);
    }

    /**
     * 查询院校封面图片列表
     * 
     * @param schoolimgs 院校封面图片
     * @return 院校封面图片
     */
    @Override
    public List<Schoolimgs> selectSchoolimgsList(Schoolimgs schoolimgs)
    {
        return schoolimgsMapper.selectSchoolimgsList(schoolimgs);
    }

    /**
     * 新增院校封面图片
     * 
     * @param schoolimgs 院校封面图片
     * @return 结果
     */
    @Override
    public int insertSchoolimgs(Schoolimgs schoolimgs)
    {
        return schoolimgsMapper.insertSchoolimgs(schoolimgs);
    }

    /**
     * 修改院校封面图片
     * 
     * @param schoolimgs 院校封面图片
     * @return 结果
     */
    @Override
    public int updateSchoolimgs(Schoolimgs schoolimgs)
    {
        return schoolimgsMapper.updateSchoolimgs(schoolimgs);
    }

    /**
     * 批量删除院校封面图片
     * 
     * @param ids 需要删除的院校封面图片主键
     * @return 结果
     */
    @Override
    public int deleteSchoolimgsByIds(String ids)
    {
        return schoolimgsMapper.deleteSchoolimgsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除院校封面图片信息
     * 
     * @param id 院校封面图片主键
     * @return 结果
     */
    @Override
    public int deleteSchoolimgsById(Long id)
    {
        return schoolimgsMapper.deleteSchoolimgsById(id);
    }
}
