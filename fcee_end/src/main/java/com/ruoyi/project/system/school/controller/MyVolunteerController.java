package com.ruoyi.project.system.school.controller;

import com.ruoyi.common.utils.PageUtils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.school.domain.SchoolInfo2022Sum;
import com.ruoyi.project.system.school.mapper.SchoolInfoSum;
import com.ruoyi.project.system.school.service.SchoolinfoSumService;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.volunteer.domain.FilingPlan;
import com.ruoyi.project.system.volunteer.domain.Piecebypiece;
import com.ruoyi.project.system.volunteer.domain.Volunteer;
import com.ruoyi.project.system.volunteer.service.IVolunteerService;
import com.ruoyi.project.system.volunteer.service.IVolunteerService2;
import com.ruoyi.project.system.volunteer.service.tool.domain.FilingPlanTool;
import com.ruoyi.project.tool.fceeTool.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/Volunteer")
public class MyVolunteerController extends  BaseController{
    @Autowired
    private IVolunteerService volunteerService;

    @Autowired
    private IVolunteerService2 volunteerService2;


    @Autowired
    private SchoolInfoSum schoolInfoSum;

    @Autowired
    private SchoolinfoSumService schoolinfoSumService;
    /*
    * 添加志愿到我的志愿单
    * VolunteerColleges;//志愿院校代码
    * Volunteering;//志愿专业代码
    * */
    @PostMapping("/AddVolunteer")
    public AjaxResult AddVolunteer(Volunteer volunteer){
        User Vuser = ShiroUtils.getSysUser(); //获取用户信息
        if (volunteer.getUserId()==null){

            String userid = String.valueOf(Vuser.getUserId());

            volunteer.setUserId(userid);

        }
        if (volunteer.getUserName()==null){
            String userName = Vuser.getUserName();
            volunteer.setUserName(userName);
        }
        int addCode = volunteerService.insertVolunteer(volunteer);
        if (addCode == 1){//判断执行结果
            return new AjaxResult(AjaxResult.Type.SUCCESS,"添加成功");
        }
        else {
            return new AjaxResult(AjaxResult.Type.ERROR,"失败");
        }

    }
/*
* 删除用户*/
    /*获取我的志愿单*/
    @GetMapping("/getVolunteer")
    public TableDataInfo getVolunteer(Volunteer volunteer){
//        BaseController baseController = new BaseController();
        PageUtils.startPage();
        User Vuser = ShiroUtils.getSysUser(); //获取用户信息
        String userid = String.valueOf(Vuser.getUserId());
        String UserName = Vuser.getUserName();
        volunteer.setUserId(userid);
        volunteer.setUserName(UserName);
        List<Volunteer> vList = volunteerService.selectVolunteerList(volunteer);
        return getDataTable(vList);
    }

    /*删除志愿单*/
    @PostMapping("/DelVolunteer")
    public  AjaxResult DelVolunteer(long id){
        startPage();
        return toAjax(volunteerService.deleteVolunteerById(id));
    }

    /*获取分数段
//    * Suject选考科目
    * Score分数*/
    @GetMapping("/getOnePice")
    public Result<Piecebypiece> getOnePice(double Score){
        startPage();
        Piecebypiece pe = new Piecebypiece();
        pe.setScore(Score);
        Piecebypiece vList = volunteerService2.getListScore(pe);
        if (vList!=null){
            return new Result<Piecebypiece>(0,vList,"成功");

        }
        else {
            return  new Result(10,"未找到");
        }


    }
  /*  physics物理
  * Chemical 华学
  *biology 生物 、
  * Thought思想
  * history历史
  * geography地理*/
    @GetMapping("/Volunteerfill")
    public TableDataInfo Volunteerfill(int Score){
        int rank;
        Piecebypiece pe = new Piecebypiece();
        pe.setScore(Score);
        Piecebypiece vList = volunteerService2.getListScore(pe);//获取分数段
        System.out.println(vList);
        FilingPlan filingPlanTool = new FilingPlan();
         rank = (int) (vList.getTnumber());
        filingPlanTool.setLowNumberPlans((long) rank);
        filingPlanTool.setLowNumberScore(Score);
        List<FilingPlanTool> list = volunteerService2.simulation(filingPlanTool);
        List<SchoolInfo2022Sum> list1 = null;




        //数据处理
        for (int i=0;i<list.size();i++){
            String SchoolCode = list.get(i).getSchoolCode();
            double listScore = list.get(i).getLowNumberScore();
            if (listScore>Score){

                list.remove(i);
            }
        }



        

        return getDataTable(list);
    }

    @GetMapping("/fillIn")
    public Result<Set> Volunteerfill(int Score,String [] suject){
        Set<SchoolInfo2022Sum> set = new HashSet<>();
        SchoolInfo2022Sum sm = new SchoolInfo2022Sum();
        Piecebypiece pe = new Piecebypiece();
        pe.setScore(Score);
        Piecebypiece ranking = volunteerService2.getListScore(pe);//获取分数段
        for (int i=0;i<suject.length;i++){
                sm.setTheLowestRankingIn2020(String.valueOf(ranking));
                sm.setTheLowestRankingIn2021(String.valueOf(ranking));
                sm.setElectiveSubjectRequirements(suject[i]);
                set.addAll( schoolinfoSumService.rank_infoAll(sm));
        }
        return new Result<Set>(0,set,"成功");
    }






}
