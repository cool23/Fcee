package com.ruoyi.project.system.school.controller;


import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.school.domain.CampusScenery;
import com.ruoyi.project.system.school.service.CampusSceneryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/CampusScenery")
public class homeSchoolInfo {//主页院校信息
    @Autowired
    private CampusSceneryService campusSceneryService;


    @GetMapping("/{SchoolCode}")
    public Set<CampusScenery> HomeCampusScenery(@PathVariable String[] SchoolCode){//院校风光
        Set<CampusScenery> all = campusSceneryService.HomeCampusScenery(SchoolCode);

        return all;
    }

    @GetMapping("/HomeCsp")
    public List<CampusScenery> HomeCsp(){

        return  campusSceneryService.HomeCSP();
    }

}
