package com.ruoyi.project.system.fceeschool2022.mapper;

import java.util.List;
import com.ruoyi.project.system.fceeschool2022.domain.Fceeschool2022;

/**
 * 2022院校信息（本科）Mapper接口
 * 
 * @author 宋伟
 * @date 2022-07-15
 */
public interface Fceeschool2022Mapper 
{
    /**
     * 查询2022院校信息（本科）
     * 
     * @param sid 2022院校信息（本科）主键
     * @return 2022院校信息（本科）
     */
    public Fceeschool2022 selectFceeschool2022BySid(Long sid);

    /**
     * 查询2022院校信息（本科）列表
     * 
     * @param fceeschool2022 2022院校信息（本科）
     * @return 2022院校信息（本科）集合
     */
    public List<Fceeschool2022> selectFceeschool2022List(Fceeschool2022 fceeschool2022);

    /**
     * 新增2022院校信息（本科）
     * 
     * @param fceeschool2022 2022院校信息（本科）
     * @return 结果
     */
    public int insertFceeschool2022(Fceeschool2022 fceeschool2022);

    /**
     * 修改2022院校信息（本科）
     * 
     * @param fceeschool2022 2022院校信息（本科）
     * @return 结果
     */
    public int updateFceeschool2022(Fceeschool2022 fceeschool2022);

    /**
     * 删除2022院校信息（本科）
     * 
     * @param sid 2022院校信息（本科）主键
     * @return 结果
     */
    public int deleteFceeschool2022BySid(Long sid);

    /**
     * 批量删除2022院校信息（本科）
     * 
     * @param sids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFceeschool2022BySids(String[] sids);
}
