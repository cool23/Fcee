package com.ruoyi.project.system.volunteer.mapper;

import java.util.List;
import com.ruoyi.project.system.volunteer.domain.Volunteer;

/**
 * 我的志愿单Mapper接口
 * 
 * @author sw
 * @date 2022-08-25
 */
public interface VolunteerMapper 
{
    /**
     * 查询我的志愿单
     * 
     * @param id 我的志愿单主键
     * @return 我的志愿单
     */
    public Volunteer selectVolunteerById(Long id);

    /**
     * 查询我的志愿单列表
     * 
     * @param volunteer 我的志愿单
     * @return 我的志愿单集合
     */
    public List<Volunteer> selectVolunteerList(Volunteer volunteer);

    /**
     * 新增我的志愿单
     * 
     * @param volunteer 我的志愿单
     * @return 结果
     */
    public int insertVolunteer(Volunteer volunteer);

    /**
     * 修改我的志愿单
     * 
     * @param volunteer 我的志愿单
     * @return 结果
     */
    public int updateVolunteer(Volunteer volunteer);

    /**
     * 删除我的志愿单
     * 
     * @param id 我的志愿单主键
     * @return 结果
     */
    public int deleteVolunteerById(Long id);

    /**
     * 批量删除我的志愿单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVolunteerByIds(String[] ids);
}
