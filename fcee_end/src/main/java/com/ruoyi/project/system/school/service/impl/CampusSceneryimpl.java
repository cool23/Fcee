package com.ruoyi.project.system.school.service.impl;

import com.ruoyi.project.system.school.domain.CampusScenery;
import com.ruoyi.project.system.school.mapper.CampusScenerymapper;
import com.ruoyi.project.system.school.service.CampusSceneryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.*;

@Service
public class CampusSceneryimpl implements CampusSceneryService {

    @Autowired
    private CampusScenerymapper campusScenerymapper;

    Random random = new Random();

    @Override
    public Set<CampusScenery> HomeCampusScenery(String[] schoolimgs) {
        Set<CampusScenery> all = new  HashSet<>();
        for (int i = 0;i<schoolimgs.length;i++){
            List<CampusScenery> comlist = campusScenerymapper.HomeCampusScenery(schoolimgs[i]);
            all.addAll(comlist);
        }
        return all;
    }

    //条数


    @Override
    public List<CampusScenery> HomeCSP() {
       List<CampusScenery> list = new ArrayList<>();
        List<CampusScenery> csp = campusScenerymapper.HomeCSP();
        for (int i=0;i<8;i++){
            list.add(csp.get(random.nextInt(csp.size())));

        }


        return csp;
    }


}
