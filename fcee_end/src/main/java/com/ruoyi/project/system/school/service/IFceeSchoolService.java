package com.ruoyi.project.system.school.service;

import java.util.List;
import com.ruoyi.project.system.school.domain.FceeSchool;
import org.springframework.stereotype.Service;

/**
 * 院校管理Service接口
 * 
 * @author 宋伟
 * @date 2022-05-22
 */
public interface IFceeSchoolService 
{
    /**
     * 查询院校管理
     * 
     * @param sid 院校管理主键
     * @return 院校管理
     */
    public FceeSchool selectFceeSchoolBySid(Long sid);

    /**
     * 查询院校管理列表
     * 
     * @param fceeSchool 院校管理
     * @return 院校管理集合
     */
    public List<FceeSchool> selectFceeSchoolList(FceeSchool fceeSchool);

    /**
     * 新增院校管理
     * 
     * @param fceeSchool 院校管理
     * @return 结果
     */
    public int insertFceeSchool(FceeSchool fceeSchool);

    /**
     * 修改院校管理
     * 
     * @param fceeSchool 院校管理
     * @return 结果
     */
    public int updateFceeSchool(FceeSchool fceeSchool);

    /**
     * 批量删除院校管理
     * 
     * @param sids 需要删除的院校管理主键集合
     * @return 结果
     */
    public int deleteFceeSchoolBySids(String sids);

    /**
     * 删除院校管理信息
     * 
     * @param sid 院校管理主键
     * @return 结果
     */
    public int deleteFceeSchoolBySid(Long sid);
}
