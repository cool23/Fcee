package com.ruoyi.project.system.school.service;

import com.ruoyi.project.system.school.domain.SchoolInfo2022Sum;

import java.util.List;
import java.util.Set;

public interface SchoolinfoSumService {
    public List<SchoolInfo2022Sum> ListByInfo(SchoolInfo2022Sum schoolInfo2022Sum);

    public List<SchoolInfo2022Sum> getSchoolAll();

    public List<SchoolInfo2022Sum> SeachSchool(SchoolInfo2022Sum schoolInfo2022Sum);


    public List<SchoolInfo2022Sum> School_infoAll(SchoolInfo2022Sum schoolInfo2022Sum);

    public Set<SchoolInfo2022Sum> rank_infoAll(SchoolInfo2022Sum schoolInfo2022Sum);
}
