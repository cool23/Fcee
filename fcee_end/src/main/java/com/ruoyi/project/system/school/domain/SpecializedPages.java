package com.ruoyi.project.system.school.domain;

import lombok.Data;

import java.util.List;

@Data
public class SpecializedPages {
    //专业页面信息
    private Integer id;
    private  String Category;//门类
    private  String Professional;//专业类
    private  String DegreeConferringCategory;//学位授予门类
    private  String Years;//修业年限

    private int pageNum;//页数
    private int pageSize;

//    private List<Specialized> SpecializedInfo;

}
