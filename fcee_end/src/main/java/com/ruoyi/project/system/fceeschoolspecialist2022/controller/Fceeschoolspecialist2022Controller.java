package com.ruoyi.project.system.fceeschoolspecialist2022.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.fceeschoolspecialist2022.domain.Fceeschoolspecialist2022;
import com.ruoyi.project.system.fceeschoolspecialist2022.service.IFceeschoolspecialist2022Service;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2022-07-15
 */
@Controller
@RequestMapping("/system/fceeschoolspecialist2022")
public class Fceeschoolspecialist2022Controller extends BaseController
{
    private String prefix = "system/fceeschoolspecialist2022";

    @Autowired
    private IFceeschoolspecialist2022Service fceeschoolspecialist2022Service;

    @RequiresPermissions("system:fceeschoolspecialist2022:view")
    @GetMapping()
    public String fceeschoolspecialist2022()
    {
        return prefix + "/fceeschoolspecialist2022";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("system:fceeschoolspecialist2022:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Fceeschoolspecialist2022 fceeschoolspecialist2022)
    {
        startPage();
        List<Fceeschoolspecialist2022> list = fceeschoolspecialist2022Service.selectFceeschoolspecialist2022List(fceeschoolspecialist2022);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("system:fceeschoolspecialist2022:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Fceeschoolspecialist2022 fceeschoolspecialist2022)
    {
        List<Fceeschoolspecialist2022> list = fceeschoolspecialist2022Service.selectFceeschoolspecialist2022List(fceeschoolspecialist2022);
        ExcelUtil<Fceeschoolspecialist2022> util = new ExcelUtil<Fceeschoolspecialist2022>(Fceeschoolspecialist2022.class);
        return util.exportExcel(list, "【请填写功能名称】数据");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("system:fceeschoolspecialist2022:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Fceeschoolspecialist2022 fceeschoolspecialist2022)
    {
        return toAjax(fceeschoolspecialist2022Service.insertFceeschoolspecialist2022(fceeschoolspecialist2022));
    }

    /**
     * 修改【请填写功能名称】
     */
    @RequiresPermissions("system:fceeschoolspecialist2022:edit")
    @GetMapping("/edit/{sid}")
    public String edit(@PathVariable("sid") Long sid, ModelMap mmap)
    {
        Fceeschoolspecialist2022 fceeschoolspecialist2022 = fceeschoolspecialist2022Service.selectFceeschoolspecialist2022BySid(sid);
        mmap.put("fceeschoolspecialist2022", fceeschoolspecialist2022);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("system:fceeschoolspecialist2022:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Fceeschoolspecialist2022 fceeschoolspecialist2022)
    {
        return toAjax(fceeschoolspecialist2022Service.updateFceeschoolspecialist2022(fceeschoolspecialist2022));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("system:fceeschoolspecialist2022:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(fceeschoolspecialist2022Service.deleteFceeschoolspecialist2022BySids(ids));
    }
}
