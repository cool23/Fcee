package com.ruoyi.project.system.school.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.school.mapper.FceeSchoolMapper;
import com.ruoyi.project.system.school.domain.FceeSchool;
import com.ruoyi.project.system.school.service.IFceeSchoolService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 院校管理Service业务层处理
 * 
 * @author 宋伟
 * @date 2022-05-22
 */
@Service
public class FceeSchoolServiceImpl implements IFceeSchoolService 
{
    @Autowired
    private FceeSchoolMapper fceeSchoolMapper;

    /**
     * 查询院校管理
     * 
     * @param sid 院校管理主键
     * @return 院校管理
     */
    @Override
    public FceeSchool selectFceeSchoolBySid(Long sid)
    {
        return fceeSchoolMapper.selectFceeSchoolBySid(sid);
    }

    /**
     * 查询院校管理列表
     * 
     * @param fceeSchool 院校管理
     * @return 院校管理
     */
    @Override
    public List<FceeSchool> selectFceeSchoolList(FceeSchool fceeSchool)
    {
        return fceeSchoolMapper.selectFceeSchoolList(fceeSchool);
    }

    /**
     * 新增院校管理
     * 
     * @param fceeSchool 院校管理
     * @return 结果
     */
    @Override
    public int insertFceeSchool(FceeSchool fceeSchool)
    {
        return fceeSchoolMapper.insertFceeSchool(fceeSchool);
    }

    /**
     * 修改院校管理
     * 
     * @param fceeSchool 院校管理
     * @return 结果
     */
    @Override
    public int updateFceeSchool(FceeSchool fceeSchool)
    {
        return fceeSchoolMapper.updateFceeSchool(fceeSchool);
    }

    /**
     * 批量删除院校管理
     * 
     * @param sids 需要删除的院校管理主键
     * @return 结果
     */
    @Override
    public int deleteFceeSchoolBySids(String sids)
    {
        return fceeSchoolMapper.deleteFceeSchoolBySids(Convert.toStrArray(sids));
    }

    /**
     * 删除院校管理信息
     * 
     * @param sid 院校管理主键
     * @return 结果
     */
    @Override
    public int deleteFceeSchoolBySid(Long sid)
    {
        return fceeSchoolMapper.deleteFceeSchoolBySid(sid);
    }
}
