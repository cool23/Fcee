package com.ruoyi.project.system.school.domain;

import lombok.Data;
//这个是专业种类表对应的实体类
@Data
public class SpecializedList {
    private Integer id;
    private  String Category;//门类
    private  String Professional;//专业类
    private  String professionalCode;//专业代码
    private  String professionaltitle;//专业名称
    private  String DegreeConferringCategory;//学位授予门类
    private  String Years;//修业年限
    private  String Addition;//增设

    private int pageNum;//页数
    private int pageSize;
}
