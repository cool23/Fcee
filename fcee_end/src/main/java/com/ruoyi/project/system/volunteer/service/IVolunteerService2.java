package com.ruoyi.project.system.volunteer.service;

import com.ruoyi.project.system.volunteer.domain.FilingPlan;
import com.ruoyi.project.system.volunteer.domain.Piecebypiece;
import com.ruoyi.project.system.volunteer.service.tool.domain.FilingPlanTool;

import java.util.List;

public interface IVolunteerService2 {
    public Piecebypiece getListScore(Piecebypiece piecebypiece);
    //模拟志愿
    public List<FilingPlanTool> simulation(FilingPlan filingPlan);
}
