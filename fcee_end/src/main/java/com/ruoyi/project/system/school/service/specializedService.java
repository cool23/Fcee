package com.ruoyi.project.system.school.service;

import com.ruoyi.project.system.school.domain.Specialized;
import com.ruoyi.project.system.school.domain.SpecializedList;
import com.ruoyi.project.system.school.domain.SpecializedPages;

import java.util.List;

public interface specializedService {
    public List<Specialized> getAll(int type);
//    public List<Specialized> Seachspecialized_Sp(String SchoolCode);

    List<Specialized> Seachspecialized_Sp(Specialized specialized);

    public  List<SpecializedPages> SpecializedHomes(SpecializedPages specializedPages);

    public  List<SpecializedList> SpecializedHomesname(SpecializedList specializedList);

}
