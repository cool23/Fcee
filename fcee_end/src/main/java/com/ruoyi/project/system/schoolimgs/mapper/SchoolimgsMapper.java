package com.ruoyi.project.system.schoolimgs.mapper;

import java.util.List;
import com.ruoyi.project.system.schoolimgs.domain.Schoolimgs;

/**
 * 院校封面图片Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-10
 */
public interface SchoolimgsMapper 
{
    /**
     * 查询院校封面图片
     * 
     * @param id 院校封面图片主键
     * @return 院校封面图片
     */
    public Schoolimgs selectSchoolimgsById(Long id);

    /**
     * 查询院校封面图片列表
     * 
     * @param schoolimgs 院校封面图片
     * @return 院校封面图片集合
     */
    public List<Schoolimgs> selectSchoolimgsList(Schoolimgs schoolimgs);

    /**
     * 新增院校封面图片
     * 
     * @param schoolimgs 院校封面图片
     * @return 结果
     */
    public int insertSchoolimgs(Schoolimgs schoolimgs);

    /**
     * 修改院校封面图片
     * 
     * @param schoolimgs 院校封面图片
     * @return 结果
     */
    public int updateSchoolimgs(Schoolimgs schoolimgs);

    /**
     * 删除院校封面图片
     * 
     * @param id 院校封面图片主键
     * @return 结果
     */
    public int deleteSchoolimgsById(Long id);

    /**
     * 批量删除院校封面图片
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSchoolimgsByIds(String[] ids);
}
