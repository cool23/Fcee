package com.ruoyi.project.system.school.mapper;

import com.ruoyi.project.system.school.domain.CampusScenery;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CampusScenerymapper {
    @Select(" select distinct fcee_school.SchoolName,schoolimgs.url from fcee_school,schoolimgs " +
            "where fcee_school.SchoolCode=(select distinct schoolimgs.SchoolCode from schoolimgs where SchoolCode=#{schoolCode}) " +
            "and schoolimgs.SchoolCode = (select distinct fcee_school.SchoolCode from schoolimgs where SchoolCode=#{schoolCode});")
    public List<CampusScenery> HomeCampusScenery(String schoolCode);

    @Select("select distinct fcee_school.SchoolName,schoolimgs.url from fcee_school,schoolimgs " +
            "where fcee_school.SchoolCode=any(select distinct schoolimgs.SchoolCode from schoolimgs ) and schoolimgs.SchoolCode = (select distinct fcee_school.SchoolCode from schoolimgs  limit 1)")
    public List<CampusScenery> HomeCSP();
}



// fcee_school.SchoolName=(select distinct schoolimgs.name from schoolimgs where SchoolCode="A001"）

//    select distinct fcee_school.SchoolName,schoolimgs.url from fcee_school,schoolimgs where fcee_school.SchoolCode=(select distinct schoolimgs.SchoolCode from schoolimgs where SchoolCode="A001") and schoolimgs.SchoolCode = (select distinct fcee_school.SchoolCode from schoolimgs where SchoolCode="A001");
//    ;