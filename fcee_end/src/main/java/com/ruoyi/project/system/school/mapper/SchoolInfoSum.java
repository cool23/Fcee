package com.ruoyi.project.system.school.mapper;

import com.ruoyi.project.system.school.domain.SchoolInfo2022Sum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

@Mapper
public interface SchoolInfoSum {
    //通过信息查询
//    select * from fceeschool2022 where SchoolName like "%山东%" and  ProfessionalName like "%计算机%";
    @Select("select * from fceeschool2022 where SchoolName like '%${SchoolName}%' " +
            "and province like '%${province}%'"+
            "and  ProfessionalName like '%${ProfessionalName}%'"+
            "and citys like '%${citys}%' "+
            "and SchoolType like '%${SchoolType}%'" +
            "and ElectiveSubjectRequirements like '%${ElectiveSubjectRequirements}%'" )

    public List<SchoolInfo2022Sum> ListByInfo(SchoolInfo2022Sum schoolInfo2022Sum);


//    cross join fceeschoolspecialist2022
    @Select("select *from schooltype")
    public List<SchoolInfo2022Sum> getSchoolAll();//获取全部院校

    @Select("select *from schooltype where schoolName like '%${schoolName}%'" +
            "and provinceCityName like '%${provinceCityName}%'" +
            "and schoolType like '%${schoolType}%'" +
            "and natureCategory like '%${natureCategory}%'")
    public List<SchoolInfo2022Sum> SeachSchool(SchoolInfo2022Sum schoolInfo2022Sum);//获取全部院校

//    fceeschoolspecialist2022.SchoolName  ,fceeschoolspecialist2022.SchoolType,
    @Select("select * from fceeschool2022  where SchoolCode like '%${SchoolCode}%' " +
            "and SchoolName like '%${SchoolName}%' and ProfessionalCode like '%${ProfessionalCode}%' and ProfessionalName like '%${ProfessionalName}%'")
    public List<SchoolInfo2022Sum> School_info_list(SchoolInfo2022Sum schoolInfo2022Sum);
    @Select("select *from fceeschoolspecialist2022 where SchoolCode like '%${SchoolCode}%' and SchoolName like '%${SchoolName}%' and ProfessionalCode like '%${ProfessionalCode}%' and ProfessionalName like '%${ProfessionalName}%' ")
    public List<SchoolInfo2022Sum> School_info_listSpecialist(SchoolInfo2022Sum schoolInfo2022Sum);



    @Select("select * from fceeschool2022  where SchoolCode like '%${SchoolCode}%' " +
            "and SchoolName like '%${SchoolName}%' and ProfessionalCode like '%${ProfessionalCode}%' and ProfessionalName like '%${ProfessionalName}%'" +
            " and ( (TheLowestRankingIn2021 <= #{TheLowestRankingIn2021}  and TheLowestRankingIn2021>= #{TheLowestRankingIn2021}+150)   and  #{TheLowestRankingIn2020}>= TheLowestRankingIn2020   )  " +
            "and ElectiveSubjectRequirements like '%${ElectiveSubjectRequirements}%' order by TheLowestRankingIn2021 desc limit 30")
    public Set<SchoolInfo2022Sum> simulation_info_list(SchoolInfo2022Sum schoolInfo2022Sum);

}
