package com.ruoyi.project.system.fceeschoolspecialist2022.mapper;

import java.util.List;
import com.ruoyi.project.system.fceeschoolspecialist2022.domain.Fceeschoolspecialist2022;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-15
 */
public interface Fceeschoolspecialist2022Mapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param sid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Fceeschoolspecialist2022 selectFceeschoolspecialist2022BySid(Long sid);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param fceeschoolspecialist2022 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Fceeschoolspecialist2022> selectFceeschoolspecialist2022List(Fceeschoolspecialist2022 fceeschoolspecialist2022);

    /**
     * 新增【请填写功能名称】
     * 
     * @param fceeschoolspecialist2022 【请填写功能名称】
     * @return 结果
     */
    public int insertFceeschoolspecialist2022(Fceeschoolspecialist2022 fceeschoolspecialist2022);

    /**
     * 修改【请填写功能名称】
     * 
     * @param fceeschoolspecialist2022 【请填写功能名称】
     * @return 结果
     */
    public int updateFceeschoolspecialist2022(Fceeschoolspecialist2022 fceeschoolspecialist2022);

    /**
     * 删除【请填写功能名称】
     * 
     * @param sid 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteFceeschoolspecialist2022BySid(Long sid);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param sids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFceeschoolspecialist2022BySids(String[] sids);
}
