package com.ruoyi.project.system.school.controller;

import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.school.domain.FceeSchool;
import com.ruoyi.project.system.school.domain.SchoolInfo2022Sum;
import com.ruoyi.project.system.school.domain.School_info;
import com.ruoyi.project.system.school.service.IFceeSchoolService;
import com.ruoyi.project.system.school.service.SchoolinfoSumService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.mybatis.logging.Logger;
import org.mybatis.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;


@RestController
@Api("院校信息接口")
@RequestMapping("/school")
public class SchoolInfo extends FceeSchoolController {

    private Logger logger = LoggerFactory.getLogger(SchoolInfo.class);

    @Autowired
    private IFceeSchoolService fceeSchoolService;

    @Autowired
    private SchoolinfoSumService schoolinfoSumService;

    @ApiOperation("院校信息查询")
    @PostMapping("/SFind")

//    @ApiParam(     "  院校代号:SchoolCode" +
//            "  院校名称:SchoolName" +
//            "  省份:province" +
//            "  专业代号:ProfessionalCode;" +
//            " 专业名称ProfessionalName;" +name= "查询",value = "查询用户列表" +
//
//            "   选考科目要求:ElectiveSubjectRequirements;\n", required = true,hidden=false)
    public TableDataInfo list( School_info school_info) {
        FceeSchool fceeSchool = new FceeSchool();//由于没装配只能new
        fceeSchool.setSchoolCode(school_info.getSchoolCode());
        fceeSchool.setSchoolName(school_info.getSchoolName());
        fceeSchool.setProvince(school_info.getProvince());
        fceeSchool.setSchoolCode(school_info.getSchoolCode());
        fceeSchool.setProfessionalName(school_info.getProfessionalName());
        fceeSchool.setElectiveSubjectRequirements(school_info.getElectiveSubjectRequirements());
        startPage();
        List<FceeSchool> list = fceeSchoolService.selectFceeSchoolList(fceeSchool);
//        System.out.println("日志：");
        System.out.println(getDataTable(Collections.singletonList("打印数据" + list)));

        return getDataTable(list);

    }



    @GetMapping("/getSchoolName")//获取全部院校
    @ResponseBody
    public TableDataInfo getAllSchoolNames(){
        startPage();
        List<SchoolInfo2022Sum> all = schoolinfoSumService.getSchoolAll();

        return getDataTable(all);
    }

    @GetMapping("/seachSchool")
    public  TableDataInfo SeachSchool(SchoolInfo2022Sum schoolInfo2022Sum){
        startPage();
        List<SchoolInfo2022Sum> all =schoolinfoSumService.SeachSchool(schoolInfo2022Sum);
        return getDataTable(all);
    }

    @GetMapping("/getSchoolinfo")
    public TableDataInfo GetSchoolInfo(SchoolInfo2022Sum schoolInfo2022Sum){
        //获取全部院校信息
        startPage();
        List<SchoolInfo2022Sum> all =schoolinfoSumService.School_infoAll(schoolInfo2022Sum);


        return getDataTable(all);
    }

}
