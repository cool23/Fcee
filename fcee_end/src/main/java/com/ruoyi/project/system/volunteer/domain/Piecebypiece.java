package com.ruoyi.project.system.volunteer.domain;

import lombok.Data;

@Data
public class Piecebypiece {
    private Integer id;
    private  double Score;//分数
    private  int TnumberH;//全体本段人数
    private  int Tnumber;//全体累计人数
    private  int physicsH;//物理本段人数
    private  int physics;//物理累计人数
    private  int ChemicalH;//化学本段人数
    private  int Chemical;//化学累计人数
    private  int biologyH;//生物本段人数
    private  int biology;//生物累计人数
    private int ThoughtH; //思想本段人数
    private  int Thought;//思想累计人数
    private  int historyH;//历史本段人数
    private  int history;//历史累计人数
    private int geographyH;//地理本段人数
    private  int geography;//地理累计人数
}
