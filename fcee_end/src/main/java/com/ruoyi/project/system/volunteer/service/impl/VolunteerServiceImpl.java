package com.ruoyi.project.system.volunteer.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.volunteer.mapper.VolunteerMapper;
import com.ruoyi.project.system.volunteer.domain.Volunteer;
import com.ruoyi.project.system.volunteer.service.IVolunteerService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 我的志愿单Service业务层处理
 * 
 * @author sw
 * @date 2022-08-25
 */
@Service
public class VolunteerServiceImpl implements IVolunteerService 
{
    @Autowired
    private VolunteerMapper volunteerMapper;

    /**
     * 查询我的志愿单
     * 
     * @param id 我的志愿单主键
     * @return 我的志愿单
     */
    @Override
    public Volunteer selectVolunteerById(Long id)
    {
        return volunteerMapper.selectVolunteerById(id);
    }

    /**
     * 查询我的志愿单列表
     * 
     * @param volunteer 我的志愿单
     * @return 我的志愿单
     */
    @Override
    public List<Volunteer> selectVolunteerList(Volunteer volunteer)
    {
        return volunteerMapper.selectVolunteerList(volunteer);
    }

    /**
     * 新增我的志愿单
     * 
     * @param volunteer 我的志愿单
     * @return 结果
     */
    @Override
    public int insertVolunteer(Volunteer volunteer)
    {
        return volunteerMapper.insertVolunteer(volunteer);
    }

    /**
     * 修改我的志愿单
     * 
     * @param volunteer 我的志愿单
     * @return 结果
     */
    @Override
    public int updateVolunteer(Volunteer volunteer)
    {
        return volunteerMapper.updateVolunteer(volunteer);
    }

    /**
     * 批量删除我的志愿单
     * 
     * @param ids 需要删除的我的志愿单主键
     * @return 结果
     */
    @Override
    public int deleteVolunteerByIds(String ids)
    {
        return volunteerMapper.deleteVolunteerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除我的志愿单信息
     * 
     * @param id 我的志愿单主键
     * @return 结果
     */
    @Override
    public int deleteVolunteerById(Long id)
    {
        return volunteerMapper.deleteVolunteerById(id);
    }
}
