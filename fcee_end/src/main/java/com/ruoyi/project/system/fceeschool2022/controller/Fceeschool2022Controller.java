package com.ruoyi.project.system.fceeschool2022.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.fceeschool2022.domain.Fceeschool2022;
import com.ruoyi.project.system.fceeschool2022.service.IFceeschool2022Service;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 2022院校信息（本科）Controller
 * 
 * @author 宋伟
 * @date 2022-07-15
 */
@Controller
@RequestMapping("/system/fceeschool2022")
public class Fceeschool2022Controller extends BaseController
{
    private String prefix = "system/fceeschool2022";

    @Autowired
    private IFceeschool2022Service fceeschool2022Service;

    @RequiresPermissions("system:fceeschool2022:view")
    @GetMapping()
    public String fceeschool2022()
    {
        return prefix + "/fceeschool2022";
    }

    /**
     * 查询2022院校信息（本科）列表
     */
    @RequiresPermissions("system:fceeschool2022:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Fceeschool2022 fceeschool2022)
    {
        startPage();
        List<Fceeschool2022> list = fceeschool2022Service.selectFceeschool2022List(fceeschool2022);
        return getDataTable(list);
    }

    /**
     * 导出2022院校信息（本科）列表
     */
    @RequiresPermissions("system:fceeschool2022:export")
    @Log(title = "2022院校信息（本科）", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Fceeschool2022 fceeschool2022)
    {
        List<Fceeschool2022> list = fceeschool2022Service.selectFceeschool2022List(fceeschool2022);
        ExcelUtil<Fceeschool2022> util = new ExcelUtil<Fceeschool2022>(Fceeschool2022.class);
        return util.exportExcel(list, "2022院校信息（本科）数据");
    }

    /**
     * 新增2022院校信息（本科）
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存2022院校信息（本科）
     */
    @RequiresPermissions("system:fceeschool2022:add")
    @Log(title = "2022院校信息（本科）", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Fceeschool2022 fceeschool2022)
    {
        return toAjax(fceeschool2022Service.insertFceeschool2022(fceeschool2022));
    }

    /**
     * 修改2022院校信息（本科）
     */
    @RequiresPermissions("system:fceeschool2022:edit")
    @GetMapping("/edit/{sid}")
    public String edit(@PathVariable("sid") Long sid, ModelMap mmap)
    {
        Fceeschool2022 fceeschool2022 = fceeschool2022Service.selectFceeschool2022BySid(sid);
        mmap.put("fceeschool2022", fceeschool2022);
        return prefix + "/edit";
    }

    /**
     * 修改保存2022院校信息（本科）
     */
    @RequiresPermissions("system:fceeschool2022:edit")
    @Log(title = "2022院校信息（本科）", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Fceeschool2022 fceeschool2022)
    {
        return toAjax(fceeschool2022Service.updateFceeschool2022(fceeschool2022));
    }

    /**
     * 删除2022院校信息（本科）
     */
    @RequiresPermissions("system:fceeschool2022:remove")
    @Log(title = "2022院校信息（本科）", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(fceeschool2022Service.deleteFceeschool2022BySids(ids));
    }
}
