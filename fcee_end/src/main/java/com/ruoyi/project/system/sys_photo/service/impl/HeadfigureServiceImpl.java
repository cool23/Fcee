package com.ruoyi.project.system.sys_photo.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.sys_photo.mapper.HeadfigureMapper;
import com.ruoyi.project.system.sys_photo.domain.Headfigure;
import com.ruoyi.project.system.sys_photo.service.IHeadfigureService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 院校图片Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-08
 */
@Service
public class HeadfigureServiceImpl implements IHeadfigureService 
{
    @Autowired
    private HeadfigureMapper headfigureMapper;

    /**
     * 查询院校图片
     * 
     * @param id 院校图片主键
     * @return 院校图片
     */
    @Override
    public Headfigure selectHeadfigureById(Long id)
    {
        return headfigureMapper.selectHeadfigureById(id);
    }

    /**
     * 查询院校图片列表
     * 
     * @param headfigure 院校图片
     * @return 院校图片
     */
    @Override
    public List<Headfigure> selectHeadfigureList(Headfigure headfigure)
    {
        return headfigureMapper.selectHeadfigureList(headfigure);
    }

    /**
     * 新增院校图片
     * 
     * @param headfigure 院校图片
     * @return 结果
     */
    @Override
    public int insertHeadfigure(Headfigure headfigure)
    {
        return headfigureMapper.insertHeadfigure(headfigure);
    }

    /**
     * 修改院校图片
     * 
     * @param headfigure 院校图片
     * @return 结果
     */
    @Override
    public int updateHeadfigure(Headfigure headfigure)
    {
        return headfigureMapper.updateHeadfigure(headfigure);
    }

    /**
     * 批量删除院校图片
     * 
     * @param ids 需要删除的院校图片主键
     * @return 结果
     */
    @Override
    public int deleteHeadfigureByIds(String ids)
    {
        return headfigureMapper.deleteHeadfigureByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除院校图片信息
     * 
     * @param id 院校图片主键
     * @return 结果
     */
    @Override
    public int deleteHeadfigureById(Long id)
    {
        return headfigureMapper.deleteHeadfigureById(id);
    }
}
