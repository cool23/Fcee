package com.ruoyi.project.system.HeadFigure.service;

import com.ruoyi.project.system.HeadFigure.domain.HP;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface hphotosService {

    public boolean save(HP hp);
    //修改
    public boolean update(HP hp);
    //删除
    public boolean del(int id);
    //查询
    public List<HP> getall();

    public HP getByid(int id);

}
