import com.ruoyi.RuoYiApplication;
import com.ruoyi.project.system.HeadFigure.domain.HP;
import com.ruoyi.project.system.HeadFigure.service.hphotosService;
import com.ruoyi.project.system.school.domain.CampusScenery;
import com.ruoyi.project.system.school.service.CampusSceneryService;
import com.ruoyi.project.system.volunteer.domain.Volunteer;
import com.ruoyi.project.system.volunteer.mapper.VolunteerMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;


@SpringBootTest(classes = RuoYiApplication.class)
@RunWith(SpringRunner.class)
public class test {
    @Autowired
    hphotosService hp;
    @Test
    public void select(){
        List<HP> all  =  hp.getall();
        System.out.println(all);

    }
    @Test

    public void save(){
        HP hp2 = new HP();

        hp2.setUrl("sdsdas");
        hp2.setFileName("sdfjj");
//        hp2.setNewFileName("sdd");
        System.out.println(hp2);
        System.out.println("hp"+hp.save(hp2));
    }
    @Autowired
    private CampusSceneryService campusSceneryService;
    @Test
    public  void Home(){
        String[] code={"A001","H001"};
        Set<CampusScenery> all = campusSceneryService.HomeCampusScenery(code);
        System.out.println(all);
    }
    @Test
    public void Homes(){
        System.out.println(campusSceneryService.HomeCSP());
    }
    @Value("${ruoyi.school_img}")
    public  String SchoolImg;
    @Test
    public void  TestValues(){
        System.out.println(SchoolImg);
    }

    @Autowired
    private VolunteerMapper myVolunteerMapper;
    @Test
    public  void  Volunteer(){
        Volunteer v = new Volunteer();
        v.setUserId("21");
        v.setVolunteerColleges("a10001");
        v.setUserName("sw");
        v.setVolunteering("213");
        System.out.println(myVolunteerMapper.insertVolunteer(v));
    }


}
