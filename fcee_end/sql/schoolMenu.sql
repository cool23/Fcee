-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校管理', '1', '1', '/system/school', 'C', '0', 'system:school:view', '#', 'admin', sysdate(), '', null, '院校管理菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校管理查询', @parentId, '1',  '#',  'F', '0', 'system:school:list',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校管理新增', @parentId, '2',  '#',  'F', '0', 'system:school:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校管理修改', @parentId, '3',  '#',  'F', '0', 'system:school:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校管理删除', @parentId, '4',  '#',  'F', '0', 'system:school:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校管理导出', @parentId, '5',  '#',  'F', '0', 'system:school:export',       '#', 'admin', sysdate(), '', null, '');
