-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校图片', '1', '1', '/system/sys_photo', 'C', '0', 'system:sys_photo:view', '#', 'admin', sysdate(), '', null, '院校图片菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校图片查询', @parentId, '1',  '#',  'F', '0', 'system:sys_photo:list',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校图片新增', @parentId, '2',  '#',  'F', '0', 'system:sys_photo:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校图片修改', @parentId, '3',  '#',  'F', '0', 'system:sys_photo:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校图片删除', @parentId, '4',  '#',  'F', '0', 'system:sys_photo:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('院校图片导出', @parentId, '5',  '#',  'F', '0', 'system:sys_photo:export',       '#', 'admin', sysdate(), '', null, '');
